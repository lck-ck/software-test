"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/14 9:31
# @Author : LuChangKai
# @File : demo02
# @Project : Utest1
"""
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.action_chains import ActionChains
driver_path = 'E:/python_workspace/Utest1/web_driver/msedgedriver.exe'
driver_serve = Service(driver_path)
driver = webdriver.Edge(service=driver_serve)
driver.maximize_window()
driver.implicitly_wait(10)
def scrollBy_by(driver,h):
    js = 'window.scrollBy(0,{});'.format(h)  # 相对滚动
    driver.execute_script(js)
    time.sleep(2)
driver.get('E:/python_workspace/Utest1/day03/frame/frame.html')
driver.implicitly_wait(10)
# 鼠标悬停
# el = driver.find_element(By.XPATH, '//*[@id="s-usersetting-top"]')

# ActionChains(driver).move_to_element(el).perform()

driver.switch_to.frame('top')

time.sleep(2)
driver.find_element(By.NAME,'message').send_keys('自动化测试')
# 回到默认框架
driver.switch_to.default_content()
driver.switch_to.frame('left')
time.sleep(2)
driver.find_element(By.NAME,'message').send_keys('性能测试')

driver.switch_to.default_content()

driver.switch_to.frame('main')
time.sleep(2)
driver.find_element(By.NAME,'message').send_keys('2003lck')