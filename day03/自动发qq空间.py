"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/14 9:45
# @Author : LuChangKai
# @File : demo03
# @Project : Utest1
"""
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.action_chains import ActionChains
driver_path = 'E:/python_workspace/Utest1/web_driver/msedgedriver.exe'
driver_serve = Service(driver_path)
driver = webdriver.Edge(service=driver_serve)
driver.maximize_window()
driver.implicitly_wait(10)
def scrollBy_by(driver,h):
    js = 'window.scrollBy(0,{});'.format(h)  # 相对滚动
    driver.execute_script(js)
    time.sleep(2)
driver.get('https://i.qq.com/')
driver.implicitly_wait(10)
driver.switch_to.frame('login_frame')

driver.find_element(By.XPATH,'//*[@id="img_out_3129437633"]').click()

driver.switch_to.default_content()
driver.find_element(By.XPATH,'//*[@id="menuContainer"]/div/ul/li[5]/a').click()


driver.find_element(By.XPATH,'//*[@id="$1_substitutor_content"]').click()

frame=driver.find_element(By.XPATH,'//*[@id="app_container"]/iframe')
driver.switch_to.frame(frame)

driver.find_element(By.XPATH,'//*[@id="qz_poster_v4_editor_container_1"]/div[1]').click()

driver.find_element(By.XPATH,'//*[@id="$1_content_content"]').send_keys('软件测试')

driver.find_element(By.XPATH,'//*[@id="QM_Mood_Poster_Container"]/div/div[4]/div[4]/a[2]/span').click()


