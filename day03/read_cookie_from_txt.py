"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/14 15:36
# @Author : LuChangKai
# @File : read_cookie_from_txt
# @Project : Utest1
"""
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.select import   Select
from selenium.webdriver.common.action_chains import ActionChains
driver_path = 'E:/python_workspace/Utest1/web_driver/msedgedriver.exe'
driver_serve = Service(driver_path)
driver = webdriver.Edge(service=driver_serve)

driver.maximize_window()
driver.get('http://192.168.217.133:81/tinyshop/')
time.sleep(5)
def scrollBy_by(driver,h):
    js = 'window.scrollBy(0,{});'.format(h)  # 相对滚动
    driver.execute_script(js)
    time.sleep(2)
f=open('cookie.txt','r',encoding='utf-8')
str=f.read()
# 字符串转cookie对象
cookie=eval(str)
driver.add_cookie(cookie)
time.sleep(2)
driver.refresh()