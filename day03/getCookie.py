"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/14 15:13
# @Author : LuChangKai
# @File : getCookie
# @Project : Utest1
"""
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.select import   Select
from selenium.webdriver.common.action_chains import ActionChains
driver_path = 'E:/python_workspace/Utest1/web_driver/msedgedriver.exe'
driver_serve = Service(driver_path)
driver = webdriver.Edge(service=driver_serve)

driver.maximize_window()
driver.get('http://192.168.217.133:81/tinyshop/')
time.sleep(30)
def scrollBy_by(driver,h):
    js = 'window.scrollBy(0,{});'.format(h)  # 相对滚动
    driver.execute_script(js)
    time.sleep(2)

# cookies=driver.get_cookies()
#
# for cookie in cookies:
#     print(cookie)
cookie=driver.get_cookie('PHPSESSID')

f=open('cookie.txt','w')
f.write(str(cookie))
f.flush()
f.close()
print('已保存')