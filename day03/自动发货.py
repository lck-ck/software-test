"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/14 11:27
# @Author : LuChangKai
# @File : demo05
# @Project : Utest1
"""
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.select import   Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.select import   Select
driver_path = 'E:/python_workspace/Utest1/web_driver/msedgedriver.exe'
driver_serve = Service(driver_path)
driver = webdriver.Edge(service=driver_serve)
driver.maximize_window()
driver.implicitly_wait(10)
driver.get('http://192.168.217.133:81/tinyshop/index.php?con=admin&act=login')

driver.find_element(By.XPATH,'//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')

driver.find_element(By.XPATH,'//*[@id="login"]/div/form/ul/li[2]/input').send_keys('admin123')

driver.find_element(By.XPATH,'//*[@id="login"]/div/form/ul/li[4]/input[1]').click()

driver.find_element(By.XPATH,'//*[@id="main_nav"]/li[2]/a').click()

driver.find_element(By.XPATH,'//*[@id="condition"]').click()

select_el=driver.find_element(By.XPATH,'//*[@id="condition_dialog"]/div[1]/span/select')
s=Select(select_el)

s.select_by_visible_text("支付状态")
s.select_by_visible_text("发货状态")

select_el1=driver.find_element(By.XPATH,'//*[@id="condition_table"]/tbody/tr[2]/td[4]/select')
s1=Select(select_el1)
s1.select_by_visible_text("已付款")

select_el2=driver.find_element(By.XPATH,'//*[@id="condition_table"]/tbody/tr[3]/td[4]/select')
s2=Select(select_el2)
s2.select_by_visible_text("未发货")
driver.find_element(By.XPATH,'//*[@id="condition_create"]').click()


# 鼠标悬停
el = driver.find_element(By.XPATH, '//*[@id="content"]/form/table/tbody/tr[2]/td[2]/div/a')

ActionChains(driver).move_to_element(el).perform()


driver.find_element(By.XPATH,'/html/body/div[3]/div[2]/form/table/tbody/tr[2]/td[2]/div/div/ul/li[2]/a').click()


frame=driver.find_element(By.XPATH,'/html/body/div[1]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[2]/td[2]/div/iframe')
driver.switch_to.frame(frame)
driver.find_element(By.XPATH,'/html/body/div/form/table/tbody/tr[3]/td[2]/input').send_keys("37892189371298371298")

driver.find_element(By.XPATH,'/html/body/div/form/div/button').click()
