"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/14 10:34
# @Author : LuChangKai
# @File : demo04
# @Project : Utest1
"""
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.select import   Select
from selenium.webdriver.common.action_chains import ActionChains
driver_path = 'E:/python_workspace/Utest1/web_driver/msedgedriver.exe'
driver_serve = Service(driver_path)
driver = webdriver.Edge(service=driver_serve)
driver.maximize_window()
driver.implicitly_wait(10)
def scrollBy_by(driver,h):
    js = 'window.scrollBy(0,{});'.format(h)  # 相对滚动
    driver.execute_script(js)
    time.sleep(2)
driver.get('E:/python_workspace/Utest1/day03/frame/select.html')
driver.implicitly_wait(10)

# 层级定位
time.sleep(2)
driver.find_element(By.ID,'status').find_element(By.XPATH,'//*[@id="status"]/option[3]').click()

# 使用select对象识别
select_el=driver.find_element(By.ID,'status')
s=Select(select_el)
# 1.通过索引
s.select_by_index(1)
# 2.通过value属性的值
time.sleep(2)
s.select_by_value("2")
# 3.通过text文本内容
s.select_by_visible_text("审核不通过")