"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/14 15:13
# @Author : LuChangKai
# @File : use_cookie
# @Project : Utest1
通过cookie绕过认证
"""
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.select import   Select
from selenium.webdriver.common.action_chains import ActionChains
driver_path = 'E:/python_workspace/Utest1/web_driver/msedgedriver.exe'
driver_serve = Service(driver_path)
driver = webdriver.Edge(service=driver_serve)

driver.maximize_window()
driver.get('http://192.168.217.133:81/tinyshop/')
time.sleep(5)
def scrollBy_by(driver,h):
    js = 'window.scrollBy(0,{});'.format(h)  # 相对滚动
    driver.execute_script(js)
    time.sleep(2)
cookie1={'domain': '192.168.217.133', 'httpOnly': False, 'name': 'safecode', 'path': '/', 'secure': False, 'value': '1'}
cookie2={'domain': '192.168.217.133', 'httpOnly': False, 'name': 'Tiny_autologin', 'path': '/', 'secure': False, 'value': '172378747aBQVUAQZUB1JSU1ZQAQMAUF0IBgIGWQYGVgQEUwYEUgI'}
cookie3={'domain': '192.168.217.133', 'httpOnly': False, 'name': 'PHPSESSID', 'path': '/', 'secure': False, 'value': 'agfgs02gl1c0q3ubqmbml79l77'}
driver.add_cookie(cookie1)
driver.add_cookie(cookie2)
driver.add_cookie(cookie3)
driver.refresh()