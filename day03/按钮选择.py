"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/14 8:22
# @Author : LuChangKai
# @File : demo01
# @Project : Utest1
"""
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.action_chains import ActionChains

# 驱动路径
driver_path = 'E:/python_workspace/Utest1/web_driver/msedgedriver.exe'
driver_serve = Service(driver_path)
driver = webdriver.Edge(service=driver_serve)
driver.maximize_window()
driver.implicitly_wait(10)
def scrollBy_by(driver,h):
    js = 'window.scrollBy(0,{});'.format(h)  # 相对滚动
    driver.execute_script(js)
    time.sleep(2)
driver.get('https://www.baidu.com')
# 鼠标悬停
el = driver.find_element(By.XPATH, '//*[@id="s-usersetting-top"]')

ActionChains(driver).move_to_element(el).perform()

time.sleep(2)
driver.find_element(By.LINK_TEXT,'高级搜索').click()

# driver.find_element(By.XPATH, '//*[@id="s-user-setting-menu"]/div/a[2]/span')

# 实战
radios=driver.find_elements(By.NAME,'q5')

for radio in radios:
    radio.click()
    time.sleep(2)

inputs=driver.find_elements(By.CLASS_NAME,'c-input')
for input in inputs:
    input.send_keys('软件测试实训')
    time.sleep(1)