"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/21 16:57
# @Author : LuChangKai
# @File : config_info
# @Project : Utest1
"""
# 读取配置文件

def get_config_info(name):

    config_file = 'E:/python_workspace/Utest1/config/config.txt'
    fp = open(config_file, 'r', encoding='utf-8')
    lines = fp.readlines()

    config_info = {}
    for line in lines:
        key = line.split(',')[0]
        value = line.split(',')[-1][:-1]
        config_info[key] = value  # 把所有的配置信息放config_info 字典

    value=config_info.get(name)
    return value

print(get_config_info('username'))