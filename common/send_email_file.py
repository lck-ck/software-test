# encoding: utf-8
# @author: newdream_daliu
# @file: send_email_file.py.py
# @time: 2021-11-16 11:17
# @desc: 发送邮件
import smtplib
from email.mime.text import MIMEText            #MIMRText()定义邮件正文
from email.mime.multipart import MIMEMultipart  #MIMEMulipart模块构造带附件
from email.header import Header            #Header()定义邮件标题
import os

def send_email(file_name):
        #发送邮件的服务器
        smtpserver = 'smtp.qq.com'
        #发送邮件用户和密码
        user ="3129437633@qq.com"
        password = "uwbbbfkcpurvdeff"         #第三方登录的密码
        #发送者
        sender = "3129437633@qq.com"
        #接收者   大刘老师的QQ：279129436
        receiver = ['3129437633@qq.com','279129436@qq.com']

        # 邮件主题
        subject = "Tinyshop电商项目 UI自动化测试报告"

        # 发送附件
        os.chdir('../reports')  # 切换当前目录
        sendfile = open(file_name, "r", encoding='utf-8').read()
        att = MIMEText(sendfile, "base64", "utf-8")
        att["Content-Type"] = "application/octet-stream"
        att.add_header('Content-Disposition', 'attachment', filename=file_name)

        content = 'Tinyshop电商项目 UI自动化测试报告、\n 本人是：计科3班 2000130307  卢常锴'
        msg = MIMEText(content, "html", "utf-8")
        msg['Subject'] = Header(subject, 'utf-8')

        msgRoot = MIMEMultipart('related')
        msgRoot['Subject'] = subject
        msgRoot.attach(att)
        msgRoot.attach(msg)

        smtp = smtplib.SMTP()
        smtp.connect(smtpserver)
        smtp.login(user, password)
        smtp.sendmail(sender, receiver, msgRoot.as_string())
        # smtp.sendmail(sender,receiver,msg.as_string())  #邮件正文
        smtp.quit()