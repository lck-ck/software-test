"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/8 19:27
# @Author : LuChangKai
# @File : 用户中心操作
# @Project : Utest1
"""
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.action_chains import ActionChains

# 驱动路径
driver_path = 'E:/python_workspace/Utest1/web_driver/chromedriver.exe'
driver_serve = Service(driver_path)
driver = webdriver.Chrome(service=driver_serve)
driver.implicitly_wait(10)
driver.get('http://192.168.217.128:81/tinyshop/')
driver.find_element(By.XPATH, '//*[@id="header"]/div[1]/div/div[2]/ul/li[7]/a[1]').click()

driver.find_element(By.XPATH, '//*[@id="account"]').send_keys("test@qq.com")
driver.find_element(By.XPATH, '//*[@id="main"]/div/div/form/ul/li[2]/input').send_keys('admin123')

driver.find_element(By.CLASS_NAME, 'btn.btn-main').click()
# 鼠标悬停操作
el = driver.find_element(By.XPATH, '//*[@id="header"]/div[1]/div/div[2]/ul/li[1]/a')
ActionChains(driver).move_to_element(el).perform()
#进入我的订单
driver.find_element(By.XPATH,'//*[@id="header"]/div[1]/div/div[2]/ul/li[1]/div/ul/li[1]/a').click()
# 点击已经收获
driver.find_element(By.XPATH,'/html/body/div[2]/div/div[2]/table/tbody/tr[2]/td[5]/a').click()
# 复制文本
el=driver.find_element(By.XPATH,'//*[@id="main"]/div/div[2]/table/tbody/tr[3]/td[1]/a').text

print(el)
#点击退货申请
driver.find_element(By.XPATH,'//*[@id="widget_sub_navs"]/div/div/ul[1]/li[2]/a').click()
driver.find_element(By.XPATH,'//*[@id="main"]/div/div[2]/div[1]/form/table/tbody/tr[1]/td[2]/input').send_keys(el)
driver.find_element(By.XPATH,'/html/body/div[2]/div/div[2]/div[1]/form/table/tbody/tr[6]/td[2]/textarea').send_keys('我要退款退款')
driver.find_element(By.XPATH,'/html/body/div[2]/div/div[2]/div[1]/form/table/tbody/tr[7]/td[2]/input').click()


