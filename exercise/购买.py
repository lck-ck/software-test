"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/7 17:43
# @Author : LuChangKai
# @File : work1
# @Project : Utest1
"""
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.action_chains import ActionChains

# 驱动路径
driver_path = 'E:/python_workspace/Utest1/web_driver/msedgedriver.exe'
driver_serve = Service(driver_path)
driver = webdriver.Edge(service=driver_serve)
driver.implicitly_wait(5)
driver.get('http://192.168.217.133:81/tinyshop/')
driver.implicitly_wait(5)
driver.find_element(By.XPATH, '//*[@id="header"]/div[1]/div/div[2]/ul/li[7]/a[1]').click()

driver.find_element(By.XPATH, '//*[@id="account"]').send_keys("test@qq.com")
driver.find_element(By.XPATH, '//*[@id="main"]/div/div/form/ul/li[2]/input').send_keys('admin123')


driver.find_element(By.CLASS_NAME, 'btn.btn-main').click()

page_source=driver.page_source
if page_source.__contains__('你好:test@qq.com - TinyShop大型电子商务系统！'):
    print('登陆成功')
    driver.find_element(By.XPATH, '//*[@id="main"]/div[3]/div[1]/div[2]/div[2]/ul/li[4]/dl/dt/a/img').click()

    driver.find_element(By.XPATH, '//*[@id="product-intro"]/div[3]/div/dl[1]/dd/ul/li[1]/span').click()

    driver.find_element(By.XPATH, '//*[@id="product-intro"]/div[3]/div/dl[2]/dd/ul/li[1]/span').click()

    driver.find_element(By.XPATH, '//*[@id="add-cart"]').click()

    def scrollBy_by(driver, h):
        js = 'window.scrollBy(0,{});'.format(h)  # 相对滚动
        driver.execute_script(js)

    scrollBy_by(driver, 300)
    # 鼠标悬停操作
    el = driver.find_element(By.XPATH, '//*[@id="shopping-cart"]')

    ActionChains(driver).move_to_element(el).perform()

    driver.find_element(By.XPATH, '//*[@id="shopping-cart"]/div/div/a').click()

    driver.find_element(By.XPATH, '//*[@id="main"]/div[2]/div[2]/p/a[2]').click()

    driver.find_element(By.XPATH, '//*[@id="main"]/div[2]/div/form/div[1]/ul/li').click()
    driver.find_element(By.XPATH, '//*[@id="main"]/div[2]/div/form/div[6]/p/input').click()
    driver.find_element(By.XPATH, '//*[@id="main"]/div[2]/form/div[3]/p/input').click()
else:
    print('登陆失败')