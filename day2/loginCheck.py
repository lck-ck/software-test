"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/7 9:14
# @Author : LuChangKai
# @File : loginCheck
# @Project : Utest1
"""
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service

# 驱动路径
driver_path = 'E:/python_workspace/Utest1/web_driver/chromedriver.exe'
driver_serve = Service(driver_path)

driver = webdriver.Chrome(service=driver_serve)
driver.get('http://47.107.178.45/tinyshop/')
time.sleep(2)
driver.find_element(By.LINK_TEXT,'登录').click()
time.sleep(2)
driver.find_element(By.NAME,'account').send_keys('test01@qq.com')
time.sleep(2)
driver.find_element(By.NAME,'password').send_keys('123456')
time.sleep(2)
driver.find_element(By.CLASS_NAME,'btn.btn-main').click()

page_source=driver.page_source
if page_source.__contains__('你好:test01 - TinyShop大型电子商务系统！'):
    print('登陆成功')
else:
    print('登陆失败')
time.sleep(2)
driver.quit()
