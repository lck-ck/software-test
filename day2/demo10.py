"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/7 15:49
# @Author : LuChangKai
# @File : demo07
# @Project : Utest1
JS
"""
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.action_chains import ActionChains

def highlight_element(driver, element):
    driver.execute_script("arguments[0].setAttribute('style', arguments[1]);",
                          element,"background:green; border:2px solid red;")
# 驱动路径
driver_path = 'E:/python_workspace/Utest1/web_driver/chromedriver.exe'
driver_serve = Service(driver_path)

driver = webdriver.Chrome(service=driver_serve)
driver.get('https://www.baidu.com')
driver.find_element(By.NAME,'wd').send_keys("新飞享")
driver.maximize_window()
driver.implicitly_wait(10)

js='window.scrollBy(0,300);' #相对滚动
driver.execute_script(js)