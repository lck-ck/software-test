"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/7 9:58
# @Author : LuChangKai
# @File : demo3
# @Project : Utest1
"""
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service

# 驱动路径
driver_path = 'E:/python_workspace/Utest1/web_driver/chromedriver.exe'
driver_serve = Service(driver_path)

driver = webdriver.Chrome(service=driver_serve)


# 浏览器操作练习
driver.get('https://www.baidu.com')
#class_name
time.sleep(2)
# driver.find_element(By.NAME,'wd').send_keys('怀化学院')
#
# time.sleep(2)
# driver.find_element(By.CLASS_NAME,'mnav.c-font-normal.c-color-t').click()

# driver.find_element(By.XPATH,'/html/body/div[1]/div[1]/div[3]/a[2]').click()

# driver.find_element(By.XPATH,'//div[3]//div[2]').click()
driver.find_element(By.XPATH,'//input[@id="kw" and @name="wd"]').send_keys("好好学习，天天向上")