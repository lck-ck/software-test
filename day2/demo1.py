"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/7 8:45
# @Author : LuChangKai
# @File : demo1
# @Project : Utest1
"""
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service

# 驱动路径
driver_path = 'E:/python_workspace/Utest1/web_driver/chromedriver.exe'
driver_serve = Service(driver_path)

driver = webdriver.Chrome(service=driver_serve)


# 浏览器操作练习
driver.get('https://www.baidu.com')
time.sleep(2)
driver.maximize_window();
time.sleep(2)
driver.find_element(By.LINK_TEXT,'登录').click()
time.sleep(2)
driver.refresh()
time.sleep(2)
driver.back()
time.sleep(2)
driver.forward()
time.sleep(2)
driver.get_screenshot_as_file("1.png")



