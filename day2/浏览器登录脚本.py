"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/7 14:28
# @Author : LuChangKai
# @File : demo04、
# @Project : Utest1
"""
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service

# 驱动路径
driver_path = 'E:/python_workspace/Utest1/web_driver/chromedriver.exe'
driver_serve = Service(driver_path)

driver = webdriver.Chrome(service=driver_serve)
driver.get('http://192.168.217.128/tinyshop/')
driver.maximize_window()
time.sleep(2)
driver.find_element(By.XPATH,'//*[@id="header"]/div[1]/div/div[2]/ul/li[7]/a[1]').click()
time.sleep(2)
driver.find_element(By.XPATH,'//*[@id="account"]').send_keys('test@qq.com')
time.sleep(2)
driver.find_element(By.XPATH,'//*[@id="main"]/div/div/form/ul/li[2]/input').send_keys('admin123')
time.sleep(2)
driver.find_element(By.XPATH,'//*[@id="main"]/div/div/form/ul/li[4]/button').click()


# 浏览器操作练习