"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/7 17:32
# @Author : LuChangKai
# @File : demo11
# @Project : Utest1
"""
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.action_chains import ActionChains

def highlight_element(driver, element):
    driver.execute_script("arguments[0].setAttribute('style', arguments[1]);",
                          element,"background:green; border:2px solid red;")
# 驱动路径
driver_path = 'E:/python_workspace/Utest1/web_driver/chromedriver.exe'
driver_serve = Service(driver_path)

driver = webdriver.Chrome(service=driver_serve)
driver.get('https://www.baidu.com')
driver.find_element(By.NAME,'wd').send_keys("新飞享")
driver.find_element(By.ID,'su').click()
js='window.scrollBy(0,700);' #相对滚动
driver.execute_script(js)
time.sleep(1)

js='window.scrollBy(0,-700);' #相对滚动
driver.execute_script(js)
time.sleep(1)

js='window.scrollBy(0,700);' #相对滚动
driver.execute_script(js)
time.sleep(1)

def scrollBy_by(driver,h):
    js = 'window.scrollBy(0,{});'.format(h)  # 相对滚动
    driver.execute_script(js)
    time.sleep(2)
scrollBy_by(driver,1000)
scrollBy_by(driver,-1000)