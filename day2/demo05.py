"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/7 14:42
# @Author : LuChangKai
# @File : demo05
# @Project : Utest1
"""
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service

# 驱动路径
driver_path = 'E:/python_workspace/Utest1/web_driver/chromedriver.exe'
driver_serve = Service(driver_path)

driver = webdriver.Chrome(service=driver_serve)
driver.get('http://www.baidu.com')
driver.maximize_window()
time.sleep(2)
driver.find_element(By.CSS_SELECTOR,'#s-top-left > a:nth-child(2)').click()
