"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/7 15:49
# @Author : LuChangKai
# @File : demo07
# @Project : Utest1
"""
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.action_chains import ActionChains
def highlight_element(driver, element):
    driver.execute_script("arguments[0].setAttribute('style', arguments[1]);",
                          element,"background:green; border:2px solid red;")
# 驱动路径
driver_path = 'E:/python_workspace/Utest1/web_driver/chromedriver.exe'
driver_serve = Service(driver_path)

driver = webdriver.Chrome(service=driver_serve)
driver.get('file:///E:/python_workspace/Utest1/html/Wait.html')
driver.maximize_window()
driver.implicitly_wait(10)

#  没有加任何等待会报错
# driver.find_element(By.ID,'b').click()
# el=driver.find_element(By.CLASS_NAME,'red_box')
# highlight_element(driver,el)
# 等待7秒钟
# driver.find_element(By.ID,'b').click()
# time.sleep(7)
# el=driver.find_element(By.CLASS_NAME,'red_box')
# highlight_element(driver,el)

# 隐式等待
driver.find_element(By.ID,'b').click()

el=driver.find_element(By.CLASS_NAME,'red_box')
highlight_element(driver,el)