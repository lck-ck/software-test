"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/7 8:45
# @Author : LuChangKai
# @File : demo1
# @Project : Utest1
"""
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service

# 驱动路径
driver_path = 'E:/python_workspace/Utest1/web_driver/chromedriver.exe'
driver_serve = Service(driver_path)

driver = webdriver.Chrome(service=driver_serve)


# 浏览器操作练习
driver.get('https://www.baidu.com')
driver.maximize_window()
time.sleep(2)
driver.find_element(By.NAME,'wd').send_keys('怀化学院')
time.sleep(2)
driver.find_element(By.ID,'su').click()
time.sleep(2)

url=driver.current_url
print(url)
title=driver.title
print(title)

#检查点
if title=='怀化学院_百度搜索':
    print('测试1成功')
else:
    print('测试1失败')
if title.__contains__('怀化学院'):
    print('测试2成功')
else:
    print('测试2失败')



