"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/7 15:49
# @Author : LuChangKai
# @File : demo07
# @Project : Utest1
"""
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service

# 驱动路径
driver_path = 'E:/python_workspace/Utest1/web_driver/chromedriver.exe'
driver_serve = Service(driver_path)

driver = webdriver.Chrome(service=driver_serve)
driver.get('http://www.baidu.com')
driver.maximize_window()
# 获取文本信息
els=driver.find_elements(By.CLASS_NAME,'title-content-title')

print(len(els))

for el in els:
    print(el.text)

elsa=driver.find_elements(By.CLASS_NAME,'mnav.c-font-normal.c-color-t')
for el in elsa:
    print(el.text)

els=driver.find_elements(By.CLASS_NAME,'mnav.c-font-normal.c-color-t')
for el in els:
    url=el.get_attribute('href')
    print(url)
