"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/7 15:49
# @Author : LuChangKai
# @File : demo07
# @Project : Utest1
"""
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.action_chains import ActionChains

# 驱动路径
driver_path = 'E:/python_workspace/Utest1/web_driver/chromedriver.exe'
driver_serve = Service(driver_path)

driver = webdriver.Chrome(service=driver_serve)
driver.get('http://www.baidu.com')
driver.maximize_window()

time.sleep(2)

# 鼠标悬停操作
el = driver.find_element(By.XPATH, '//*[@id="s-usersetting-top"]')

ActionChains(driver).move_to_element(el).perform()

time.sleep(2)
driver.find_element(By.LINK_TEXT,'高级搜索').click()
