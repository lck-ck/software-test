"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/7 15:36
# @Author : LuChangKai
# @File : demo06
# @Project : Utest1
元素常见操作
"""
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service

# 驱动路径
driver_path = 'E:/python_workspace/Utest1/web_driver/chromedriver.exe'
driver_serve = Service(driver_path)

driver = webdriver.Chrome(service=driver_serve)
driver.get('http://www.baidu.com')
driver.maximize_window()

time.sleep(2)
driver.find_element(By.NAME,'wd').send_keys('哈哈哈哈哈哈')
time.sleep(2)
driver.find_element(By.NAME,'wd').clear()
time.sleep(2)
driver.find_element(By.NAME,'wd').send_keys("怀化学院")
time.sleep(2)
driver.find_element(By.ID,'form').submit()
