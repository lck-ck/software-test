"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/14 16:37
# @Author : LuChangKai
# @File : case01
# @Project : Utest1
"""
# 导入unittest模块
import unittest
class TestCase01(unittest.TestCase):
    def setUp(self) -> None:
        print('前置条件')
    def tearDown(self) -> None:
        print("后置脚本")
        # 所有测试方法以test打头
    def test_case01(self):
        print('执行第一条测试用例')
        num=3+3
        self.assertEqual(num,6)  #实际结果等于预期结果
    def test_case02(self):
        print('执行第二条测试用例')
        self.assertEqual(20-10,20)
if __name__=='__main__':
    unittest.main()