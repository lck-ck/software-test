"""
# _*_ coding : utf_8 _*_
# @Time : 2022/10/31 16:51
# @Author : LuChangKai
# @File : baidu
# @Project : Utest1
"""
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service

# 驱动路径
driver_path = 'E:/python_workspace/Utest1/web_driver/msedgedriver.exe'
driver_serve = Service(driver_path)

driver = webdriver.Edge(service=driver_serve)

driver.get('https://www.baidu.com')

driver.maximize_window()
time.sleep(2)

driver.find_element(By.NAME,'wd').send_keys('新梦想')
time.sleep(2)
driver.find_element(By.ID,'su').click()
