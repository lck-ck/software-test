"""
# _*_ coding : utf_8 _*_
# @Time : 2022/10/31 16:51
# @Author : LuChangKai
# @File : baidu
# @Project : Utest1
"""
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service

# 驱动路径
driver_path = 'E:/python_workspace/Utest1/web_driver/chromedriver.exe'
driver_serve = Service(driver_path)

driver = webdriver.Chrome(service=driver_serve)

driver.get('http://47.107.178.45/phpwind/')

driver.maximize_window()
time.sleep(2)
driver.find_element(By.LINK_TEXT,'立即注册').click()
time.sleep(2)
driver.find_element(By.NAME,'username').send_keys('zxb123')
time.sleep(2)
driver.find_element(By.NAME,'password').send_keys('123456')
time.sleep(2)
driver.find_element(By.NAME,'repassword').send_keys('123456')
time.sleep(2)
driver.find_element(By.NAME,'email').send_keys('5345324325435@qq.com')
time.sleep(2)
driver.find_element(By.XPATH,'//*[@id="J_register_form"]/div/dl[5]/dd/button').click()
