# _*_ coding : utf_8 _*_
# @Time : 2022/10/31 14:38
# @Author : LuChangKai
# @File : demo01
# @Project : Utest1
age = 20
money = 999.99
print(money)
is_not = 1 == 1
if is_not:
    print('true')
else:
    print('假')
c = 3 + 4j
print(c)

str = 'LuChangKai'
print(str[0:2], str[2:7], str[7:])

stu_list = ['a', 'b', 'c']
print(stu_list)
print(stu_list[0])
print(stu_list[1])
print(stu_list[2])
# 元组 有一些配置不允许进行修改
stu_tup = ('a', 'b', 'c')
print(stu_tup)
# stu_tup[1]='32'
info = ('湖南软测信息', 'www.hhxmxit.com')
print(info)


#集合   无序的数据
stu_set={'zhangshan',"zhangshan","lisi","wangwu"}
print(stu_set)

#字典
stu_dic={'stu01':'zhangshan','stu02':'wangwu'}


print(stu_dic.get("stu01"))
for key in stu_dic.keys():
    print(key,":",stu_dic[key])