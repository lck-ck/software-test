"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/21 14:49
# @Author : LuChangKai
# @File : demo03
# @Project : Utest1
"""
import time

# print(time.time())
t1=(2023,12,21,12,0,0,0,0,0)
print(time.strftime('%Y-%m-%d %H-%M-%S',t1))

file_name=time.strftime('%Y_%m_%d_%H_%M_%S')
file_name='result_'+file_name+'.html'
print(file_name)
