"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/21 10:28
# @Author : LuChangKai
# @File : demo01
# @Project : Utest1
"""
import time
import random
import unittest

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.select import Select


class Demo1(unittest.TestCase):
    def setUp(self) -> None:
        print("前置脚本")

    def tearDown(self) -> None:
        print("后置脚本")

    def test_add(self):
        num = 5 + 5
        self.assertEqual(num, 10, "加法检查错误")

    def test_add2(self):
        num = 5 + 4
        self.assertEqual(num, 10, "加法检查错误")

if __name__ == '__main__':
    unittest.main()
