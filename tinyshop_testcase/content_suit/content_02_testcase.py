import time
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
class AdvertisementManagementTestCase(unittest.TestCase):
    def setUp(self) -> None:
        driver_path='D:/workspace/python/web_driver/chromedriver.exe'
        driver_server=Service(driver_path)
        self.driver=webdriver.Chrome(service=driver_server)
        self.driver.get('http://127.0.0.1/tinyshop/index.php?con=admin&act=login')
        self.driver.maximize_window()
        self.driver.implicitly_wait(10)
        # pass
    def tearDown(self) -> None:
        time.sleep(3)
        self.driver.quit()

    def test_add_navigation_pass(self):
        '''后台导航管理 导航添加通过测试'''
        # 登录
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('021522')
        time.sleep(2)
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        #点击内容管理
        self.driver.find_element(By.XPATH,'//*[@id="main_nav"]/li[6]/a').click()
        #点击导航管理
        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[3]/ul/li[4]/a').click()
        #点击添加
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div[1]/a[3]').click()
        #填写导航名称
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[1]/dd/input').send_keys('我是导航')
        #填写导航链接地址
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[2]/dd/input').send_keys('1')
        #提交
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/div/input[1]').click()

        self.assertTrue(self.driver.page_source.__contains__(''))

    def test_navigation_name_is_null(self):
        '''后台导航管理 导航名字为空'''
        # 登录
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('021522')
        time.sleep(2)
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        #点击内容管理
        self.driver.find_element(By.XPATH,'//*[@id="main_nav"]/li[6]/a').click()
        #点击导航管理
        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[3]/ul/li[4]/a').click()
        #点击添加
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div[1]/a[3]').click()
        # #填写导航名称
        # self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[1]/dd/input').send_keys('我是导航')
        #填写导航链接地址
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[2]/dd/input').send_keys('1')
        #提交
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/div/input[1]').click()

        self.assertTrue(self.driver.page_source.__contains__(''))

    def test_navigation_address_is_null(self):
        '''后台导航管理 导航地址为空'''
        # 登录
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('021522')
        time.sleep(2)
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        #点击内容管理
        self.driver.find_element(By.XPATH,'//*[@id="main_nav"]/li[6]/a').click()
        #点击导航管理
        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[3]/ul/li[4]/a').click()
        #点击添加
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div[1]/a[3]').click()
        #填写导航名称
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[1]/dd/input').send_keys('我是导航')
        # #填写导航链接地址
        # self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[2]/dd/input').send_keys('1')
        #提交
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/div/input[1]').click()

        self.assertEqual(self.driver.page_source.__contains__('绝对地址或相对地址(如:/ucenter/index)'),True)


    def test_delete_navigation_pass(self):
        '''后台导航管理 导航删除通过测试'''
        # 登录
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('021522')
        time.sleep(2)
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        #点击内容管理
        self.driver.find_element(By.XPATH,'//*[@id="main_nav"]/li[6]/a').click()
        #点击导航管理
        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[3]/ul/li[4]/a').click()
        # 点击全选
        self.driver.find_element(By.XPATH, '// *[ @ id = "content"] / form / div[1] / a[1]').click()
        #点击删除
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div[1]/a[2]').click()
        #确定
        self.driver.find_element(By.XPATH, '/html/body/div[1]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[3]/td/div/button[1]').click()

        self.assertTrue(self.driver.page_source.__contains__(''))

    def test_delete_navigation_is_null(self):
        '''后台导航管理 未选择要删除的导航'''
        # 登录
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('021522')
        time.sleep(2)
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        #点击内容管理
        self.driver.find_element(By.XPATH,'//*[@id="main_nav"]/li[6]/a').click()
        #点击导航管理
        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[3]/ul/li[4]/a').click()
        # # 点击全选
        # self.driver.find_element(By.XPATH, '// *[ @ id = "content"] / form / div[1] / a[1]').click()
        #点击删除
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div[1]/a[2]').click()
        #确定
        self.driver.find_element(By.XPATH, '/html/body/div[1]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[3]/td/div/button[1]').click()

        self.assertEqual(self.driver.page_source.__contains__('没有选择任何项目，无法删除'),True)
if __name__ =='__main__':
    unittest.main()