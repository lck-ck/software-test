import time
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
class AdvertisementManagementTestCase(unittest.TestCase):
    def setUp(self) -> None:
        driver_path='D:/workspace/python/web_driver/chromedriver.exe'
        driver_server=Service(driver_path)
        self.driver=webdriver.Chrome(service=driver_server)
        self.driver.get('http://127.0.0.1/tinyshop/index.php?con=admin&act=login')
        self.driver.maximize_window()
        self.driver.implicitly_wait(10)
        # pass
    def tearDown(self) -> None:
        time.sleep(3)
        self.driver.quit()

    def test_add_advertisement_pass(self):
        '''后台广告添加通过测试'''
        # 登录
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('021522')
        time.sleep(2)
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        #点击内容管理
        self.driver.find_element(By.XPATH,'//*[@id="main_nav"]/li[6]/a').click()
        #点击广告管理
        self.driver.find_element(By.XPATH, '// *[ @ id = "sidebar"] / ul / li[3] / ul / li[2] / a').click()
        #点击添加
        self.driver.find_element(By.XPATH, '// *[ @ id = "content"] / form / div / a[2]').click()
        #填写广告信息
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/dl[1]/dd/input').send_keys('双十二促销')
        #广告尺寸
        self.driver.find_element(By.XPATH, '// *[ @ id = "obj_form"] / dl[2] / dd / input[1]').send_keys('960')

        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/dl[2]/dd/input[2]').send_keys('410')
        #广告时间
        self.driver.find_element(By.XPATH, '// *[ @ id = "obj_form"] / dl[3] / dd / input').send_keys('2022-12-11 23:59')

        self.driver.find_element(By.XPATH, '// *[ @ id = "obj_form"] / dl[4] / dd / input').send_keys('2022-12-31 23:59')
        # time.sleep(10)
        #开启
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/dl[1]/dd/input').click()
        self.driver.find_element(By.XPATH, '// *[ @ id = "obj_form"] / dl[5] / dd / label / input').click()
        #描述
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/dl[6]/dd/textarea').send_keys('一年一度的双十二购物节来啦！')
        #图片地址
        self.driver.find_element(By.XPATH, '//*[@id="table_img"]/tbody/tr/td[2]/button').click()
        self.driver.switch_to.frame('Openupimg_dialog')
        self.driver.find_element(By.XPATH, '/ html / body / div[2] / ul / li[2]').click()

        self.driver.find_element(By.XPATH, '/ html / body / div[2] / div / div[2] / ul / li[1] / img').click()

        self.driver.find_element(By.XPATH, '/ html / body / div[3] / button').click()
        #提交
        self.driver.find_element(By.XPATH, '// *[ @ id = "content"] / form / div[2] / input[1]').click()

        self.assertTrue(self.driver.page_source.__contains__(''))

    def test_add_advertisement_name_is_null1(self):
        '''后台添加广告 广告名字为空'''
        # 登录
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('021522')
        time.sleep(2)
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        # 点击内容管理
        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[6]/a').click()
        # 点击广告管理
        self.driver.find_element(By.XPATH, '// *[ @ id = "sidebar"] / ul / li[3] / ul / li[2] / a').click()
        # 点击添加
        self.driver.find_element(By.XPATH, '// *[ @ id = "content"] / form / div / a[2]').click()
        # 填写广告信息
        # self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/dl[1]/dd/input').send_keys('双十二促销')
        # 广告尺寸
        self.driver.find_element(By.XPATH, '// *[ @ id = "obj_form"] / dl[2] / dd / input[1]').send_keys('960')

        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/dl[2]/dd/input[2]').send_keys('410')
        # 广告时间
        self.driver.find_element(By.XPATH, '// *[ @ id = "obj_form"] / dl[3] / dd / input').send_keys(
            '2022-12-11 23:59')

        self.driver.find_element(By.XPATH, '// *[ @ id = "obj_form"] / dl[4] / dd / input').send_keys(
            '2022-12-31 23:59')
        # time.sleep(10)
        # 开启
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/dl[1]/dd/input').click()
        self.driver.find_element(By.XPATH, '// *[ @ id = "obj_form"] / dl[5] / dd / label / input').click()
        # 描述
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/dl[6]/dd/textarea').send_keys('一年一度的双十二购物节来啦！')
        # 图片地址
        self.driver.find_element(By.XPATH, '//*[@id="table_img"]/tbody/tr/td[2]/button').click()
        self.driver.switch_to.frame('Openupimg_dialog')
        self.driver.find_element(By.XPATH, '/ html / body / div[2] / ul / li[2]').click()

        self.driver.find_element(By.XPATH, '/ html / body / div[2] / div / div[2] / ul / li[1] / img').click()

        self.driver.find_element(By.XPATH, '/ html / body / div[3] / button').click()
        # 提交
        self.driver.find_element(By.XPATH, '// *[ @ id = "content"] / form / div[2] / input[1]').click()

        self.assertEqual(self.driver.page_source.__contains__('广告名称不能为空！'),True)

    def test_add_advertisement_picture_is_null(self):
        '''后台添加广告 图片为空'''
        # 登录
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('021522')
        time.sleep(2)
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        # 点击内容管理
        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[6]/a').click()
        # 点击广告管理
        self.driver.find_element(By.XPATH, '// *[ @ id = "sidebar"] / ul / li[3] / ul / li[2] / a').click()
        # 点击添加
        self.driver.find_element(By.XPATH, '// *[ @ id = "content"] / form / div / a[2]').click()
        # 填写广告信息
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/dl[1]/dd/input').send_keys('双十二促销')
        # 广告尺寸
        self.driver.find_element(By.XPATH, '// *[ @ id = "obj_form"] / dl[2] / dd / input[1]').send_keys('960')

        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/dl[2]/dd/input[2]').send_keys('410')
        # 广告时间
        self.driver.find_element(By.XPATH, '// *[ @ id = "obj_form"] / dl[3] / dd / input').send_keys(
            '2022-12-11 23:59')

        self.driver.find_element(By.XPATH, '// *[ @ id = "obj_form"] / dl[4] / dd / input').send_keys(
            '2022-12-31 23:59')
        # time.sleep(10)
        # 开启
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/dl[1]/dd/input').click()
        self.driver.find_element(By.XPATH, '// *[ @ id = "obj_form"] / dl[5] / dd / label / input').click()
        # 描述
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/dl[6]/dd/textarea').send_keys('一年一度的双十二购物节来啦！')
        # 图片地址
        self.driver.find_element(By.XPATH, '//*[@id="table_img"]/tbody/tr/td[2]/button').click()
        self.driver.switch_to.frame('Openupimg_dialog')
        self.driver.find_element(By.XPATH, '/ html / body / div[2] / ul / li[2]').click()

        # self.driver.find_element(By.XPATH, '/ html / body / div[2] / div / div[2] / ul / li[1] / img').click()
        self.driver.find_element(By.XPATH, '/ html / body / div[3] / button').click()
        self.assertEqual(self.driver.page_source.__contains__('未选择任何图片，无法添加！'),True)


    def test_add_advertisement_color_is_wrong(self):
        '''后台添加广告 广告类型为文字时颜色格式不正确'''
        # 登录
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('021522')
        time.sleep(2)
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        # 点击内容管理
        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[6]/a').click()
        # 点击广告管理
        self.driver.find_element(By.XPATH, '// *[ @ id = "sidebar"] / ul / li[3] / ul / li[2] / a').click()
        # 点击添加
        self.driver.find_element(By.XPATH, '// *[ @ id = "content"] / form / div / a[2]').click()
        # 填写广告信息
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/dl[1]/dd/input').send_keys('双十二促销')
        # 广告尺寸
        self.driver.find_element(By.XPATH, '// *[ @ id = "obj_form"] / dl[2] / dd / input[1]').send_keys('960')

        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/dl[2]/dd/input[2]').send_keys('410')
        # 广告时间
        self.driver.find_element(By.XPATH, '// *[ @ id = "obj_form"] / dl[3] / dd / input').send_keys('2022-12-11 23:59')

        self.driver.find_element(By.XPATH, '// *[ @ id = "obj_form"] / dl[4] / dd / input').send_keys('2022-12-31 23:59')
        # time.sleep(10)
        # 开启
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/dl[1]/dd/input').click()
        self.driver.find_element(By.XPATH, '// *[ @ id = "obj_form"] / dl[5] / dd / label / input').click()
        # 描述
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/dl[6]/dd/textarea').send_keys('一年一度的双十二购物节来啦！')
        #选择广告类型
        self.driver.find_element(By.XPATH,'//*[@id="ad_type"]/option[3]').click()
        self.driver.find_element(By.XPATH, '// *[ @ id = "table_font"] / tbody / tr / td[2] / input').send_keys('hello')
        #填写颜色类型

        self.driver.find_element(By.XPATH, '// *[ @ id = "table_font"] / tbody / tr / td[6] / input').send_keys('1')
        # # 图片地址
        # self.driver.find_element(By.XPATH, '//*[@id="table_img"]/tbody/tr/td[2]/button').click()
        # self.driver.switch_to.frame('Openupimg_dialog')
        # self.driver.find_element(By.XPATH, '/ html / body / div[2] / ul / li[2]').click()
        #
        # self.driver.find_element(By.XPATH, '/ html / body / div[2] / div / div[2] / ul / li[1] / img').click()
        #
        # self.driver.find_element(By.XPATH, '/ html / body / div[3] / button').click()
        # 提交
        self.driver.find_element(By.XPATH, '// *[ @ id = "content"] / form / div[2] / input[1]').click()

        self.assertEqual(self.driver.page_source.__contains__('十六进制颜色(如：#019010)'),True)

    def test_add_advertisement_code_is_null(self):
        '''后台添加广告 广告类型为代码时代码内容为空'''
        # 登录
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('021522')
        time.sleep(2)
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        # 点击内容管理
        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[6]/a').click()
        # 点击广告管理
        self.driver.find_element(By.XPATH, '// *[ @ id = "sidebar"] / ul / li[3] / ul / li[2] / a').click()
        # 点击添加
        self.driver.find_element(By.XPATH, '// *[ @ id = "content"] / form / div / a[2]').click()
        # 填写广告信息
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/dl[1]/dd/input').send_keys('双十二促销')
        # 广告尺寸
        self.driver.find_element(By.XPATH, '// *[ @ id = "obj_form"] / dl[2] / dd / input[1]').send_keys('960')

        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/dl[2]/dd/input[2]').send_keys('410')
        # 广告时间
        self.driver.find_element(By.XPATH, '// *[ @ id = "obj_form"] / dl[3] / dd / input').send_keys('2022-12-11 23:59')

        self.driver.find_element(By.XPATH, '// *[ @ id = "obj_form"] / dl[4] / dd / input').send_keys('2022-12-31 23:59')
        # time.sleep(10)
        # 开启
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/dl[1]/dd/input').click()
        self.driver.find_element(By.XPATH, '// *[ @ id = "obj_form"] / dl[5] / dd / label / input').click()
        # 描述
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/dl[6]/dd/textarea').send_keys('一年一度的双十二购物节来啦！')
        #选择广告类型
        self.driver.find_element(By.XPATH,'//*[@id="ad_type"]/option[5]').click()
        # #填写颜色类型
        # self.driver.find_element(By.XPATH, '// *[ @ id = "table_font"] / tbody / tr / td[6] / input').send_keys('1')
        # # 图片地址
        # self.driver.find_element(By.XPATH, '//*[@id="table_img"]/tbody/tr/td[2]/button').click()
        # self.driver.switch_to.frame('Openupimg_dialog')
        # self.driver.find_element(By.XPATH, '/ html / body / div[2] / ul / li[2]').click()
        #
        # self.driver.find_element(By.XPATH, '/ html / body / div[2] / div / div[2] / ul / li[1] / img').click()
        #
        # self.driver.find_element(By.XPATH, '/ html / body / div[3] / button').click()
        # 提交
        self.driver.find_element(By.XPATH, '// *[ @ id = "content"] / form / div[2] / input[1]').click()

        self.assertEqual(self.driver.page_source.__contains__('代码内容不能为空'),True)
if __name__ =='__main__':
    unittest.main()
