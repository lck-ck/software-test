"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/17 9:27
# @Author : LuChangKai
# @File : order01_testcase
# @Project : Utest1
"""
import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.select import Select
# 订单中心-快递点配置-快递单模板（此处运行可能会报错，还存在bug）
import unittest


class order_05_Testcase(unittest.TestCase):
    def setUp(self) -> None:
        # self.driver_path='E:/python_workspace/Utest1/web_driver/chromedriver.exe'
        # self.driver_server=Service(self.driver_path)

        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(10)

        self.driver.get('http://192.168.217.133:81/tinyshop/index.php?con=admin&act=login')

        self.driver.maximize_window()

    def tearDown(self) -> None:
        time.sleep(2)
        self.driver.quit()

    # 1.通过测试用例
    def test_order_pass(self):
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('admin123')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()

        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[2]/a').click()

        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[3]/ul/li[2]/a').click()

        self.driver.find_element(By.XPATH,'//*[@id="content"]/form/div/a[3]').click()

        # 添加快递单模板名称
        self.driver.find_element(By.XPATH,'//*[@id="inputForm"]/div[1]/input[1]').send_keys('1')
        # 悬停按钮
        # 鼠标悬停
        el = self.driver.find_element(By.XPATH, '//*[@id="inputForm"]/div[2]/div/a[1]')

        ActionChains(self.driver).move_to_element(el).perform()
        # 添加标签
        self.driver.find_element(By.XPATH,'//*[@id="tagOption"]/li[2]/a').click()
        time.sleep(2)
        # 保存模板
        self.driver.find_element(By.XPATH,'//*[@id="deleteTag"]').click()

        # 等于断言
        self.assertEqual(self.driver.page_source.__contains__('123'), True)


if __name__ == '__main__':
    unittest.main()
