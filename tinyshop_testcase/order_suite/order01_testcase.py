"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/17 9:27
# @Author : LuChangKai
# @File : order01_testcase
# @Project : Utest1
订单中心-商品订单-发货脚本编写
"""
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.select import Select
# 前台登录的测试用例
import unittest


class order_01_Testcase(unittest.TestCase):
    def setUp(self) -> None:
        # self.driver_path='E:/python_workspace/Utest1/web_driver/chromedriver.exe'
        # self.driver_server=Service(self.driver_path)

        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(10)

        self.driver.get('http://192.168.217.133:81/tinyshop/index.php?con=admin&act=login')

        self.driver.maximize_window()

    def tearDown(self) -> None:
        self.driver.quit()

    # 1.通过测试用例
    def test_order_pass(self):
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('admin123')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()

        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[2]/a').click()

        self.driver.find_element(By.XPATH, '//*[@id="condition"]').click()

        select_el = self.driver.find_element(By.XPATH, '//*[@id="condition_dialog"]/div[1]/span/select')
        s = Select(select_el)

        s.select_by_visible_text("支付状态")
        s.select_by_visible_text("发货状态")

        select_el1 = self.driver.find_element(By.XPATH, '//*[@id="condition_table"]/tbody/tr[2]/td[4]/select')
        s1 = Select(select_el1)
        s1.select_by_visible_text("已付款")

        select_el2 = self.driver.find_element(By.XPATH, '//*[@id="condition_table"]/tbody/tr[3]/td[4]/select')
        s2 = Select(select_el2)
        s2.select_by_visible_text("未发货")
        self.driver.find_element(By.XPATH, '//*[@id="condition_create"]').click()

        # 鼠标悬停
        el = self.driver.find_element(By.XPATH, '//*[@id="content"]/form/table/tbody/tr[2]/td[2]/div/a')

        ActionChains(self.driver).move_to_element(el).perform()

        self.driver.find_element(By.XPATH,
                                 '/html/body/div[3]/div[2]/form/table/tbody/tr[2]/td[2]/div/div/ul/li[2]/a').click()

        frame = self.driver.find_element(By.XPATH,
                                         '/html/body/div[1]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[2]/td[2]/div/iframe')
        self.driver.switch_to.frame(frame)
        self.driver.find_element(By.XPATH, '/html/body/div/form/table/tbody/tr[3]/td[2]/input').send_keys(
            "37892189371298371298")

        self.driver.find_element(By.XPATH, '/html/body/div/form/div/button').click()
        # 等于断言
        self.assertEqual(self.driver.page_source.__contains__('已发货'), True)


if __name__ == '__main__':
    unittest.main()
