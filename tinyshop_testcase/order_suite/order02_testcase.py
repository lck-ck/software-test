"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/17 9:27
# @Author : LuChangKai
# @File : order01_testcase
# @Project : Utest1
"""
import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.select import Select
# 订单中心-退款单
import unittest


class order_02_Testcase(unittest.TestCase):
    def setUp(self) -> None:
        # self.driver_path='E:/python_workspace/Utest1/web_driver/chromedriver.exe'
        # self.driver_server=Service(self.driver_path)

        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(10)

        self.driver.get('http://192.168.217.133:81/tinyshop/index.php?con=admin&act=login')

        self.driver.maximize_window()

    def tearDown(self) -> None:
        self.driver.quit()

    # 1.通过测试用例
    def test_order02_pass(self):

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('admin123')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()

        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[2]/a').click()

        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[2]/ul/li[4]/a').click()


        # 鼠标悬停
        el = self.driver.find_element(By.XPATH, '//*[@id="content"]/form/table/tbody/tr[3]/td[2]/div/a')

        ActionChains(self.driver).move_to_element(el).perform()

        self.driver.find_element(By.XPATH,
                                 '// *[ @ id = "content"] / form / table / tbody / tr[3] / td[2] / div / div / ul / li[2] / a').click()
        frame = self.driver.find_element(By.XPATH,'/html/body/div[1]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[2]/td[2]/div/iframe')
        self.driver.switch_to.frame(frame)


       # 输入退款渠道
        self.driver.find_element(By.XPATH,"/html/body/div/form/table/tbody/tr[13]/td/input").send_keys('支付宝')
        # 退款到账号
        self.driver.find_element(By.XPATH,"/html/body/div/form/table/tbody/tr[14]/td[1]/input").send_keys('123456')
        # 处置意见
        self.driver.find_element(By.XPATH,"/html/body/div/form/table/tbody/tr[16]/td/textarea").send_keys('退钱啦')

        # self.driver.find_element(By.XPATH, '/html/body/div/form/div/button').click()
        # 等于断言
        self.assertEqual(self.driver.page_source.__contains__('退款成功'), True)

        # 2.未写退款方式
    def order_login_error(self):
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('admin123')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()

        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[2]/a').click()

        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[2]/ul/li[4]/a').click()

        # 鼠标悬停
        el = self.driver.find_element(By.XPATH, '//*[@id="content"]/form/table/tbody/tr[3]/td[2]/div/a')

        ActionChains(self.driver).move_to_element(el).perform()

        self.driver.find_element(By.XPATH,
                                 '// *[ @ id = "content"] / form / table / tbody / tr[3] / td[2] / div / div / ul / li[2] / a').click()
        frame = self.driver.find_element(By.XPATH,
                                         '/html/body/div[1]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[2]/td[2]/div/iframe')
        self.driver.switch_to.frame(frame)

        # 输入退款渠道
        self.driver.find_element(By.XPATH, "/html/body/div/form/table/tbody/tr[13]/td/input").send_keys('')
        # 退款到账号
        self.driver.find_element(By.XPATH, "/html/body/div/form/table/tbody/tr[14]/td[1]/input").send_keys('123456')
        # 处置意见
        self.driver.find_element(By.XPATH, "/html/body/div/form/table/tbody/tr[16]/td/textarea").send_keys('退钱啦')

        # self.driver.find_element(By.XPATH, '/html/body/div/form/div/button').click()
        # 等于断言
        self.assertEqual(self.driver.page_source.__contains__('退款渠道不能为空'), True)

if __name__ == '__main__':
    unittest.main()
