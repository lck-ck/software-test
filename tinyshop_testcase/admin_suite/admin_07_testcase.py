#邮箱配置
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By

class admin_02_testcase(unittest.TestCase):

    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        url = 'http://127.0.0.1/tinyshop/index.php?con=admin&act=config_other'
        self.driver.get(url)
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('123456')
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        #邮箱配置
        self.driver.find_element(By.XPATH,'//*[@id="sidebar"]/ul/li[1]/ul/li[6]/a').click()


    def tearDown(self) -> None:
        time.sleep(1)
        self.driver.quit()

    def test_stmpAdress(self):
        self.driver.find_element(By.XPATH,'//*[@id="content"]/div/form/dl[2]/dd/input').clear()
        self.driver.find_element(By.XPATH, '//*[@id="content"]/div/form/dl[2]/dd/input').send_keys('smtp.gmail.com')
        self.driver.find_element(By.XPATH,'//*[@id="content"]/div/form/div/input').click()
        self.assertEqual(self.driver.page_source.__contains__('信息保存成功'), True)

    def test_port(self):
        self.driver.find_element(By.XPATH,'//*[@id="textfield"]').clear()
        self.driver.find_element(By.XPATH, '//*[@id="textfield"]').send_keys('465')
        self.driver.find_element(By.XPATH,'//*[@id="content"]/div/form/div/input').click()
        self.assertEqual(self.driver.page_source.__contains__('信息保存成功'), True)

    def test_emailAddress(self):
        self.driver.find_element(By.NAME,'email_account').clear()
        self.driver.find_element(By.NAME, 'email_account').send_keys('123@qq.com')
        self.driver.find_element(By.XPATH,'//*[@id="content"]/div/form/div/input').click()
        self.assertEqual(self.driver.page_source.__contains__('信息保存成功'), True)

    def test_emailpassword(self):
        self.driver.find_element(By.NAME,'email_password').clear()
        self.driver.find_element(By.NAME, 'email_password').send_keys('123')
        self.driver.find_element(By.XPATH,'//*[@id="content"]/div/form/div/input').click()
        self.assertEqual(self.driver.page_source.__contains__('信息保存成功'), True)

    def test_sendName(self):
        self.driver.find_element(By.NAME,'email_sender_name').clear()
        self.driver.find_element(By.NAME, 'email_sender_name').send_keys('admin')
        self.driver.find_element(By.XPATH,'//*[@id="content"]/div/form/div/input').click()
        self.assertEqual(self.driver.page_source.__contains__('信息保存成功'), True)

    def test_reciveName(self):
        self.driver.find_element(By.NAME,'test_email').clear()
        self.driver.find_element(By.NAME, 'test_email').send_keys('怀化学院')
        self.driver.find_element(By.XPATH,'//*[@id="content"]/div/form/div/input').click()
        self.assertEqual(self.driver.page_source.__contains__('信息保存成功'), True)




if __name__ == '__main__':
        unittest.main()


