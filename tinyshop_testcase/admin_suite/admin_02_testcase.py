#其他配置
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By

class admin_02_testcase(unittest.TestCase):

    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        url = 'http://127.0.0.1/tinyshop/index.php?con=admin&act=config_other'
        self.driver.get(url)
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('123456')
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        self.driver.find_element(By.XPATH,'//*[@id="sidebar"]/ul/li[1]/ul/li[5]/a').click()
        time.sleep(1)

    def tearDown(self) -> None:
        time.sleep(1)
        self.driver.quit()

    def test_no_rate(self):
        '''税率没有填写'''
        self.driver.find_element(By.XPATH, '//*[@id="content"]/div/form/dl[6]/dd/input').clear()
        self.driver.find_element(By.XPATH, '//*[@id="content"]/div/form/div/input').click()
        self.assertEqual(self.driver.page_source.__contains__(''), True)

    def test_no_consumetime(self):
        '''空的或者出现汉字'''
        self.driver.find_element(By.XPATH, '//*[@id="content"]/div/form/dl[8]/dd/input').send_keys('汉字')
        self.driver.find_element(By.XPATH, '//*[@id="content"]/div/form/div/input').click()
        self.assertEqual(self.driver.page_source.__contains__(''), True)


    def test_no_Snappedtime(self):
        '''空的或者出现汉字'''
        self.driver.find_element(By.XPATH, '//*[@id="content"]/div/form/dl[9]/dd/input').send_keys('汉字')
        self.driver.find_element(By.XPATH, '//*[@id="content"]/div/form/div/input').click()
        self.assertEqual(self.driver.page_source.__contains__(''), True)


    def test_no_Buyime(self):
        '''空的或者出现汉字'''
        self.driver.find_element(By.XPATH, '//*[@id="content"]/div/form/dl[10]/dd/input').send_keys('汉字')
        self.driver.find_element(By.XPATH, '//*[@id="content"]/div/form/div/input').click()
        self.assertEqual(self.driver.page_source.__contains__(''), True)



    def test_no_bindingtime(self):
        '''空的或者出现汉字'''
        self.driver.find_element(By.XPATH, '//*[@id="content"]/div/form/dl[11]/dd/input').send_keys('汉字')
        self.driver.find_element(By.XPATH, '//*[@id="content"]/div/form/div/input').click()
        self.assertEqual(self.driver.page_source.__contains__(''), True)

    def test_no_refund(self):
        '''空的或者出现汉字'''
        self.driver.find_element(By.XPATH, '//*[@id="content"]/div/form/dl[11]/dd/input').send_keys('汉字')
        self.driver.find_element(By.XPATH, '//*[@id="content"]/div/form/div/input').click()
        self.assertEqual(self.driver.page_source.__contains__(''), True)

    def test_no_voidtime(self):
        '''空的或者出现汉字'''
        self.driver.find_element(By.XPATH, '//*[@id="content"]/div/form/dl[12]/dd/input').send_keys('汉字')
        self.driver.find_element(By.XPATH, '//*[@id="content"]/div/form/div/input').click()
        self.assertEqual(self.driver.page_source.__contains__(''), True)


if __name__ == '__main__':
        unittest.main()