#修改系统管理员密码
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By

class admin_02_testcase(unittest.TestCase):

    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        url = 'http://127.0.0.1/tinyshop/index.php?con=admin&act=config_other'
        self.driver.get(url)
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('123456')
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        #安全管理
        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[4]/ul/li[1]/b/a').click()
        #管理员
        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[4]/ul/li[2]/a').click()
        #处理
        self.driver.find_element(By.XPATH,'//*[@id="content"]/form/table/tbody/tr[3]/td[2]/div/a').click()
        #编辑
        self.driver.find_element(By.XPATH,'//*[@id="content"]/form/table/tbody/tr[3]/td[2]/div/div/ul/li[1]/a').click()

        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/dl[3]/dd/label/a').click()

    def tearDown(self) -> None:
        time.sleep(1)
        self.driver.quit()

    def test_modify_pass(self):
        '''修改密码成功'''
        self.driver.find_element(By.XPATH,'//*[@id="password"]').send_keys('123456')
        self.driver.find_element(By.XPATH,'//*[@id="repassword"]').send_keys('123456')
        self.driver.find_element(By.XPATH,'//*[@id="password_info"]/form/div/input[1]').click()
        self.driver.find_element(By.XPATH,'//*[@id="password_info"]/form/div/input[1]').click()
        self.assertEqual(self.driver.page_source.__contains__('密码修改成功'), True)

    def test_no_password(self):
        '''没有输入新密码'''
        # self.driver.find_element(By.XPATH,'//*[@id="password"]').send_keys('123456')
        self.driver.find_element(By.XPATH,'//*[@id="repassword"]').send_keys('123456')
        self.driver.find_element(By.XPATH,'//*[@id="password_info"]/form/div/input[1]').click()
        self.driver.find_element(By.XPATH,'//*[@id="password_info"]/form/div/input[1]').click()
        self.assertEqual(self.driver.page_source.__contains__(''), True)

    def test_modify_reepeatPassword(self):
        '''没有再次输入密码'''
        self.driver.find_element(By.XPATH,'//*[@id="password"]').send_keys('123456')
        self.driver.find_element(By.XPATH,'//*[@id="repassword"]').send_keys('123456')
        self.driver.find_element(By.XPATH,'//*[@id="password_info"]/form/div/input[1]').click()
        # self.driver.find_element(By.XPATH,'//*[@id="password_info"]/form/div/input[1]').click()
        self.assertEqual(self.driver.page_source.__contains__(''), True)

    def test_password_different(self):
        '''俩次输入密码不一样'''
        self.driver.find_element(By.XPATH,'//*[@id="password"]').send_keys('123456')
        self.driver.find_element(By.XPATH,'//*[@id="repassword"]').send_keys('1234568')
        self.driver.find_element(By.XPATH,'//*[@id="password_info"]/form/div/input[1]').click()
        self.driver.find_element(By.XPATH,'//*[@id="password_info"]/form/div/input[1]').click()
        self.assertEqual(self.driver.page_source.__contains__('两次输入密码不一致'), True)

    def test_no_options(self):
        '''什么也你要填写'''
        # self.driver.find_element(By.XPATH,'//*[@id="password"]').send_keys('123456')
        # self.driver.find_element(By.XPATH,'//*[@id="repassword"]').send_keys('1234568')
        self.driver.find_element(By.XPATH,'//*[@id="password_info"]/form/div/input[1]').click()
        # self.driver.find_element(By.XPATH,'//*[@id="password_info"]/form/div/input[1]').click()
        self.assertEqual(self.driver.page_source.__contains__(''), True)

if __name__ == '__main__':
        unittest.main()