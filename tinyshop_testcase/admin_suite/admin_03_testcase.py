#增加系统管理员
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By

class admin_02_testcase(unittest.TestCase):

    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        url = 'http://127.0.0.1/tinyshop/index.php?con=admin&act=config_other'
        self.driver.get(url)
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('123456')
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        self.driver.find_element(By.XPATH,'//*[@id="sidebar"]/ul/li[4]/ul/li[1]/b/a').click()
        self.driver.find_element(By.XPATH,'//*[@id="sidebar"]/ul/li[4]/ul/li[2]/a').click()
        self.driver.find_element(By.XPATH,'//*[@id="content"]/form/div/a[2]').click()


    def tearDown(self) -> None:
        time.sleep(1)
        self.driver.quit()

    def test_add_pass(self):
        '''添加管理员成功'''
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/dl[1]/dd/input').send_keys(time.time())
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/dl[3]/dd/input').send_keys('123456')
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/dl[4]/dd/input').send_keys('123456')
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/div/input[1]').click()
        self.assertEqual(self.driver.page_source.__contains__('添加'), True)

    def test_no_username(self):
        '''用户名为空'''
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/dl[1]/dd/input').send_keys()
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/dl[3]/dd/input').send_keys('123456')
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/dl[4]/dd/input').send_keys('123456')
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/div/input[1]').click()
        self.assertEqual(self.driver.page_source.__contains__('用户名不能为空'), True)

    def test_no_password(self):
        '''密码少于六位'''
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/dl[1]/dd/input').send_keys(time.time())
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/dl[3]/dd/input').send_keys()
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/dl[4]/dd/input').send_keys('123456')
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/div/input[1]').click()
        self.assertEqual(self.driver.page_source.__contains__('密码必需大于6位'), True)

    def test_no_repeatPassword(self):
        '''确认密码少于六位'''
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/dl[1]/dd/input').send_keys(time.time())
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/dl[3]/dd/input').send_keys('123456')
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/dl[4]/dd/input').send_keys()
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/div/input[1]').click()
        self.assertEqual(self.driver.page_source.__contains__('密码必需大于6位'), True)

    def test_password_different(self):
        '''俩次输入密码不一致'''
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/dl[1]/dd/input').send_keys(time.time())
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/dl[3]/dd/input').send_keys('123456')
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/dl[4]/dd/input').send_keys('123456789')
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/div/input[1]').click()
        self.assertEqual(self.driver.page_source.__contains__('两次输入密码不一致'), True)



if __name__ == '__main__':
        unittest.main()