#管理页面
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By

class admin_01_testcase(unittest.TestCase):

    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        url = 'http://127.0.0.1/tinyshop/index.php?con=admin&act=index'
        self.driver.get(url)
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('123456')
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        time.sleep(1)

    def tearDown(self) -> None:
        time.sleep(1)
        self.driver.quit()

    def test_order(self):
        '''待发货订单'''
        self.driver.find_element(By.XPATH,'//*[@id="content"]/div/div[2]/table/tbody/tr[1]/td[1]/a').click()
        self.assertEqual(self.driver.page_source.__contains__('筛选条件'), True)

    def test_police(self):
        '''商品库存警告'''
        self.driver.find_element(By.XPATH,'//*[@id="content"]/div/div[2]/table/tbody/tr[2]/td[1]/a').click()
        self.assertEqual(self.driver.page_source.__contains__('筛选条件'), True)

    def test_Refundnote(self):
        '''待处理的退款单'''
        self.driver.find_element(By.XPATH,'//*[@id="content"]/div/div[2]/table/tbody/tr[3]/td[1]/a').click()
        self.assertEqual(self.driver.page_source.__contains__('筛选条件'), True)

if __name__ == '__main__':
        unittest.main()

