#站点设置
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By

class admin_02_testcase(unittest.TestCase):

    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        url = 'http://127.0.0.1/tinyshop/index.php?con=admin&act=config_other'
        self.driver.get(url)
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('123456')
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        #站点设置
        self.driver.find_element(By.XPATH,'//*[@id="sidebar"]/ul/li[1]/ul/li[4]/a').click()

    def tearDown(self) -> None:
        time.sleep(1)
        self.driver.quit()


    def test_compare(self):
        self.driver.find_element(By.XPATH,'//*[@id="content"]/div/form/dl[5]/dd/input').clear()
        self.driver.find_element(By.XPATH, '//*[@id="content"]/div/form/dl[5]/dd/input').send_keys('洛洛有限公司')
        self.driver.find_element(By.XPATH,'//*[@id="content"]/div/form/div/input').click()
        self.assertEqual(self.driver.page_source.__contains__('信息保存成功'), True)

    def test_netAddress(self):
        self.driver.find_element(By.XPATH,'//*[@id="content"]/div/form/dl[6]/dd/input').clear()
        self.driver.find_element(By.XPATH, '//*[@id="content"]/div/form/dl[6]/dd/input').send_keys('怀化学院')
        self.driver.find_element(By.XPATH, '//*[@id="content"]/div/form/div/input').click()
        self.assertEqual(self.driver.page_source.__contains__('信息保存成功'), True)


    def test_email(self):
        self.driver.find_element(By.XPATH,'//*[@id="content"]/div/form/dl[7]/dd/input').clear()
        self.driver.find_element(By.XPATH, '//*[@id="content"]/div/form/dl[7]/dd/input').send_keys('168796@qq.com')
        self.driver.find_element(By.XPATH, '//*[@id="content"]/div/form/div/input').click()
        self.assertEqual(self.driver.page_source.__contains__('信息保存成功'), True)

    def test_phone(self):
        self.driver.find_element(By.XPATH,'//*[@id="content"]/div/form/dl[8]/dd/input').clear()
        self.driver.find_element(By.XPATH, '//*[@id="content"]/div/form/dl[8]/dd/input').send_keys('1559993')
        self.driver.find_element(By.XPATH, '//*[@id="content"]/div/form/div/input').click()
        self.assertEqual(self.driver.page_source.__contains__('信息保存成功'), True)

    def test_phoneNumber(self):
        self.driver.find_element(By.XPATH,'//*[@id="content"]/div/form/dl[9]/dd/input').clear()
        self.driver.find_element(By.XPATH, '//*[@id="content"]/div/form/dl[9]/dd/input').send_keys('1559993')
        self.driver.find_element(By.XPATH, '//*[@id="content"]/div/form/div/input').click()
        self.assertEqual(self.driver.page_source.__contains__('信息保存成功'), True)

    def test_code(self):
        self.driver.find_element(By.XPATH,'//*[@id="content"]/div/form/dl[10]/dd/input').clear()
        self.driver.find_element(By.XPATH, '//*[@id="content"]/div/form/dl[10]/dd/input').send_keys('4222000')
        self.driver.find_element(By.XPATH, '//*[@id="content"]/div/form/div/input').click()
        self.assertEqual(self.driver.page_source.__contains__('信息保存成功'), True)

    def test_adress(self):
        self.driver.find_element(By.XPATH,'//*[@id="content"]/div/form/dl[11]/dd/input').clear()
        self.driver.find_element(By.XPATH, '//*[@id="content"]/div/form/dl[11]/dd/input').send_keys('1559993')
        self.driver.find_element(By.XPATH, '//*[@id="content"]/div/form/div/input').click()
        self.assertEqual(self.driver.page_source.__contains__('信息保存成功'), True)



if __name__ == '__main__':
        unittest.main()

