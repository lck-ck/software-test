#操作日志
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By

class admin_02_testcase(unittest.TestCase):

    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        url = 'http://127.0.0.1/tinyshop/index.php?con=admin&act=config_other'
        self.driver.get(url)
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('123456')
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        #安全管理
        self.driver.find_element(By.XPATH,'//*[@id="sidebar"]/ul/li[4]/ul/li[1]/b/a').click()
        #操作日志
        self.driver.find_element(By.XPATH,'//*[@id="sidebar"]/ul/li[4]/ul/li[5]/a').click()



    def tearDown(self) -> None:
        time.sleep(1)
        self.driver.quit()


    def test_alldelete(self):
        '''全部删除'''
        self.driver.find_element(By.XPATH,'//*[@id="content"]/form/div[1]/a[1]').click()
        self.driver.find_element(By.XPATH,'//*[@id="content"]/form/div[1]/a[2]').click()
        self.driver.find_element(By.XPATH,'/html/body/div[1]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[3]/td/div/button[1]').click()
        self.assertEqual(self.driver.page_source.__contains__(''), True)

if __name__ == '__main__':
        unittest.main()