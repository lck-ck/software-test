"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/17 9:35
# @Author : LuChangKai
# @File : customer02_testcase
# @Project : Utest1
  @description: 会员等级管理
"""
import time
import random
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.select import Select


def get_random_str(randomlength=16):
    random_str = ''
    base_str = 'ABCDEFGHIGKLMNOPORSTUVWXYZabcdefghigklmnopqrstuvwxyz0123456789'
    length = len(base_str) - 1
    for i in range(randomlength):
        random_str += base_str[random.randint(0, length)]
    return random_str


# 前台登录的测试用例
import unittest


class addvip_customer_Testcase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(10)

        self.driver.get('http://192.168.217.133:81/tinyshop/index.php?con=admin&act=login')

        self.driver.maximize_window()

    def tearDown(self) -> None:
        time.sleep(2)
        self.driver.quit()

    '''通过测试'''
    def test_add_vip_pass(self):


        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('admin123')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[3]/a').click()
        # 点击客户中心
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div/a[3]').click()
        # 进入会员等级管理

        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[1]/ul/li[3]/a').click()
        # 点击添加按钮
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div/a[3]').click()
        # 会员名填写
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/dl[1]/dd/input').send_keys('VIP')
        # 消费金额填写
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/dl[2]/dd/input').send_keys('10000')
        # 提交
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/div/input[1]').click()
        # 等于断言
        self.assertEqual(self.driver.page_source.__contains__('VIP'), True)
        pass
    # 等级名为空
    def test_add_vip_rankName_isNull(self):


        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('admin123')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[3]/a').click()
        # 点击客户中心
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div/a[3]').click()
        # 进入会员等级管理

        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[1]/ul/li[3]/a').click()
        # 点击添加按钮
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div/a[3]').click()
        # 会员名填写
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/dl[1]/dd/input').send_keys('')
        # 消费金额填写
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/dl[2]/dd/input').send_keys('10000')
        # 提交
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/div/input[1]').click()
        # 等于断言
        self.assertEqual(self.driver.page_source.__contains__(''), True)
        pass
    def test_add_vip_money_isNull(self):


        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('admin123')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[3]/a').click()
        # 点击客户中心
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div/a[3]').click()
        # 进入会员等级管理

        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[1]/ul/li[3]/a').click()
        # 点击添加按钮
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div/a[3]').click()
        # 会员名填写
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/dl[1]/dd/input').send_keys('SVIP')
        # 消费金额填写
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/dl[2]/dd/input').send_keys('saaaa')
        # 提交
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/div/input[1]').click()
        # 等于断言
        self.assertEqual(self.driver.page_source.__contains__('必需为整数数字'), True)
        pass



if __name__ == '__main__':
    unittest.main()
