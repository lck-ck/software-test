"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/17 9:35
# @Author : LuChangKai
# @File : customer04_testcase
# @Project : Utest1
  @description: 删除商品评价
"""
import time
import random
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.action_chains import ActionChains


def get_random_str(randomlength=16):
    random_str = ''
    base_str = 'ABCDEFGHIGKLMNOPORSTUVWXYZabcdefghigklmnopqrstuvwxyz0123456789'
    length = len(base_str) - 1
    for i in range(randomlength):
        random_str += base_str[random.randint(0, length)]
    return random_str


# 前台登录的测试用例
import unittest


class deletecomment_customer_Testcase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(10)

        self.driver.get('http://192.168.217.133:81/tinyshop/index.php?con=admin&act=login')

        self.driver.maximize_window()

    def tearDown(self) -> None:
        time.sleep(2)
        self.driver.quit()

    '''通过测试'''
    def test_delete_comment_pass(self):
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('admin123')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[3]/a').click()
        # 点击客户中心
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div/a[3]').click()
        # 进入商品评价
        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[3]/ul/li[2]/a').click()
        # 鼠标悬停
        el = self.driver.find_element(By.XPATH, '//*[@id="content"]/form/table/tbody/tr[2]/td[2]')

        ActionChains(self.driver).move_to_element(el).perform()
        # 点击删除
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/table/tbody/tr[2]/td[2]/div/div/ul/li/a').click()
        # 确认删除
        self.driver.find_element(By.XPATH, '/html/body/div[1]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[3]/td/div/button[1]').click()
        # 等于断言
        self.assertEqual(self.driver.page_source.__contains__(''), True)
        pass
    # 取消删除
    def test_delete_comment_error(self):
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('admin123')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[3]/a').click()
        # 点击客户中心
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div/a[3]').click()
        # 进入商品评价
        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[3]/ul/li[2]/a').click()
        # 鼠标悬停
        el = self.driver.find_element(By.XPATH, '//*[@id="content"]/form/table/tbody/tr[2]/td[2]')

        ActionChains(self.driver).move_to_element(el).perform()
        # 点击删除
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/table/tbody/tr[2]/td[2]/div/div/ul/li/a').click()
        # 确认删除
        self.driver.find_element(By.XPATH, '/html/body/div[1]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[3]/td/div/button[2]').click()
        # 等于断言
        self.assertEqual(self.driver.page_source.__contains__('无法恢复'), True)
        pass




if __name__ == '__main__':
    unittest.main()
