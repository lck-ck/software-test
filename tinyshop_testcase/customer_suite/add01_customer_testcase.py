"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/17 9:35
# @Author : LuChangKai
# @File : add01_testcase
# @Project : Utest1
  @description: 自动化测试
"""
import time
import random
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.select import Select


def get_random_str(randomlength=16):
    random_str = ''
    base_str = 'ABCDEFGHIGKLMNOPORSTUVWXYZabcdefghigklmnopqrstuvwxyz0123456789'
    length = len(base_str) - 1
    for i in range(randomlength):
        random_str += base_str[random.randint(0, length)]
    return random_str


# 前台登录的测试用例
import unittest


class add01_customer_Testcase(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(10)

        self.driver.get('http://192.168.217.133:81/tinyshop/index.php?con=admin&act=login')

        self.driver.maximize_window()

    def tearDown(self) -> None:
        time.sleep(2)
        self.driver.quit()

    # 1.通过测试用例，正常添加一个客户
    # 2.打开菜单，并点击增加会员
    # 3.在新增页面输入高中操作并点击新增
    def test_add_customer_pass(self):
        '''通过测试'''
        # 后台登陆
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('admin123')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[3]/a').click()
        # 点击客户中心
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div/a[3]').click()
        # 账号
        name = get_random_str(5)
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[1]/dd/input').send_keys(name)

        # 密码
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[2]/dd/input').send_keys('12345678')
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[3]/dd/input').send_keys('12345678')
        # 邮箱
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[4]/dd/input').send_keys(
            name + '433289478@qq.com')
        # 姓名
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[6]/dd/input').send_keys('自动化测试')
        # 性别
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[7]/dd/input[2]').click()
        # 生日
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[8]/dd/input').send_keys('2022-07-30')
        # 地区
        select_el = self.driver.find_element(By.ID, 'province')
        s = Select(select_el)
        s.select_by_visible_text("江西省")

        select_el = self.driver.find_element(By.ID, 'city')
        s = Select(select_el)
        s.select_by_visible_text("南昌市")

        select_el = self.driver.find_element(By.ID, 'county')
        s = Select(select_el)
        s.select_by_visible_text("东湖区")

        # 详细地址
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[10]/dd/input').send_keys('1234567890')
        # 固化
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[11]/dd/input').send_keys('1234567890')
        # 手机
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[12]/dd/input').send_keys('15296325447')
        # 提交
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/div/input[1]').click()

        # 等于断言
        self.assertEqual(self.driver.page_source.__contains__('自动化测试'), True)
        pass

        # 添加客户，真实姓名为空
    def test_add_customer_real_name_is_null(self):
        '''真实姓名为空'''
        # 后台登陆
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('admin123')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[3]/a').click()

        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div/a[3]').click()
        # 账号
        name = get_random_str(5)
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[1]/dd/input').send_keys(name)

        # 密码
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[2]/dd/input').send_keys('12345678')
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[3]/dd/input').send_keys('12345678')
        # 邮箱
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[4]/dd/input').send_keys('433289478@qq.com')
        # 姓名
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[6]/dd/input').send_keys('')
        # 性别
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[7]/dd/input[2]').click()
        # 生日
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[8]/dd/input').send_keys('2022-07-30')
        # 地区
        select_el = self.driver.find_element(By.ID, 'province')
        s = Select(select_el)
        s.select_by_visible_text("江西省")

        select_el = self.driver.find_element(By.ID, 'city')
        s = Select(select_el)
        s.select_by_visible_text("南昌市")

        select_el = self.driver.find_element(By.ID, 'county')
        s = Select(select_el)
        s.select_by_visible_text("东湖区")

        # 详细地址
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[10]/dd/input').send_keys('1234567890')
        # 固化
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[11]/dd/input').send_keys('1234567890')
        # 手机
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[12]/dd/input').send_keys('15296325447')
        # 提交
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/div/input[1]').click()
        # 断言
        self.assertEqual(self.driver.page_source.__contains__('必填'), True)
        pass
        '''
            用户名为空
        '''
    def test_add_customer_user_isNull(self):
        '''用户名为空'''
        # 后台登陆
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('admin123')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[3]/a').click()

        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div/a[3]').click()
        # 账号

        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[1]/dd/input').send_keys('')

        # 密码
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[2]/dd/input').send_keys('12345678')
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[3]/dd/input').send_keys('12345678')
        # 邮箱
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[4]/dd/input').send_keys('433289478@qq.com')
        # 姓名
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[6]/dd/input').send_keys('自动化测试')
        # 性别
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[7]/dd/input[2]').click()
        # 生日
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[8]/dd/input').send_keys('2022-07-30')
        # 地区
        select_el = self.driver.find_element(By.ID, 'province')
        s = Select(select_el)
        s.select_by_visible_text("江西省")

        select_el = self.driver.find_element(By.ID, 'city')
        s = Select(select_el)
        s.select_by_visible_text("南昌市")

        select_el = self.driver.find_element(By.ID, 'county')
        s = Select(select_el)
        s.select_by_visible_text("东湖区")

        # 详细地址
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[10]/dd/input').send_keys('1234567890')
        # 固化
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[11]/dd/input').send_keys('1234567890')
        # 手机
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[12]/dd/input').send_keys('15296325447')
        # 提交
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/div/input[1]').click()
        # 断言
        self.assertEqual(self.driver.page_source.__contains__('必填'), True)
        pass
    def test_add_customer_notEmail(self):
        '''邮箱为空'''
        # 后台登陆
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('admin123')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[3]/a').click()
        # 点击客户中心
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div/a[3]').click()
        # 账号
        name = get_random_str(5)
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[1]/dd/input').send_keys(name)

        # 密码
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[2]/dd/input').send_keys('12345678')
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[3]/dd/input').send_keys('12345678')
        # 邮箱

        # 姓名
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[6]/dd/input').send_keys('自动化测试')
        # 性别
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[7]/dd/input[2]').click()
        # 生日
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[8]/dd/input').send_keys('2022-07-30')
        # 地区
        select_el = self.driver.find_element(By.ID, 'province')
        s = Select(select_el)
        s.select_by_visible_text("江西省")

        select_el = self.driver.find_element(By.ID, 'city')
        s = Select(select_el)
        s.select_by_visible_text("南昌市")

        select_el = self.driver.find_element(By.ID, 'county')
        s = Select(select_el)
        s.select_by_visible_text("东湖区")

        # 详细地址
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[10]/dd/input').send_keys('1234567890')
        # 固化
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[11]/dd/input').send_keys('1234567890')
        # 手机
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[12]/dd/input').send_keys('15296325447')
        # 提交
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/div/input[1]').click()

        # 等于断言
        self.assertEqual(self.driver.page_source.__contains__('邮箱地址(例如:tiny@tiny.com)'), True)
        pass
    def test_add_customer_password_isIllegal(self):
        '''密码不合法'''
        # 后台登陆
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('admin123')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[3]/a').click()

        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div/a[3]').click()
        # 账号

        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[1]/dd/input').send_keys('')

        # 密码
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[2]/dd/input').send_keys('123')
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[3]/dd/input').send_keys('123')
        # 邮箱
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[4]/dd/input').send_keys('433289478@qq.com')
        # 姓名
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[6]/dd/input').send_keys('自动化测试')
        # 性别
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[7]/dd/input[2]').click()
        # 生日
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[8]/dd/input').send_keys('2022-07-30')
        # 地区
        select_el = self.driver.find_element(By.ID, 'province')
        s = Select(select_el)
        s.select_by_visible_text("江西省")

        select_el = self.driver.find_element(By.ID, 'city')
        s = Select(select_el)
        s.select_by_visible_text("南昌市")

        select_el = self.driver.find_element(By.ID, 'county')
        s = Select(select_el)
        s.select_by_visible_text("东湖区")

        # 详细地址
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[10]/dd/input').send_keys('1234567890')
        # 固化
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[11]/dd/input').send_keys('1234567890')
        # 手机

        # 提交
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/div/input[1]').click()
        # 断言
        self.assertEqual(self.driver.page_source.__contains__('必填'), True)
        pass

if __name__ == '__main__':
    unittest.main()
