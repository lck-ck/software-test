import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from Common import config_info

class Ucenter_info_TestCase(unittest.TestCase):
    def setUp(self) -> None:
      self.driver=webdriver.Chrome()
      self.driver.get(config_info.get_config_info('url2'))
      self.driver.maximize_window()
      self.driver.implicitly_wait(10)

    def tearDown(self) -> None:
        time.sleep(3)
        self.driver.quit()

    def test_login_pass(self):
      self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')
      self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('021522')
      self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
      time.sleep(2)
      self.driver.find_element(By.XPATH, '// *[ @ id = "main_nav"] / li[5] / a').click()
      self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[2]/ul/li[2]/a').click()
      self.driver.find_element(By.XPATH, '//*[@id="datepick"]').click()
      self.driver.find_element(By.XPATH, '/html/body/div[3]/div[3]/ul/li[5]').click()
      self.driver.find_element(By.XPATH, '/html/body/div[3]/div[3]/div/button[1]').click()
      #self.driver.find_element(By.XPATH, '//*[@id="condition"]').click()
if __name__ == '__main__':
    unittest.main()
