import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service


class message_list_TestCase(unittest.TestCase):
    def setUp(self) -> None:
        driver_path = 'E:/PyCharm File/RuanJIanCeiShi/project/web_driver/chromedriver.exe'
        driver_server = Service(driver_path)
        self.driver = webdriver.Chrome(service=driver_server)
        self.driver.get('http://127.0.0.1/tinyshop/index.php?con=admin&act=login')
        self.driver.maximize_window()
        self.driver.implicitly_wait(10)

    def tearDown(self) -> None:
        time.sleep(2)
        self.driver.quit()

    def test_Message_sending_pass(self):
        "客户中心信息管理——信息发送"
        # 登录

        time.sleep(1)
        self.driver.find_element(By.NAME, 'name').send_keys('admin')
        time.sleep(1)
        self.driver.find_element(By.NAME, 'password').send_keys('123456')
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()

        # 客户中心
        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[3]/a').click()
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[3]/ul/li[4]/a').click()
        time.sleep(1)
        # 信息发送
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div/a[3]').click()
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[1]/dd/input').send_keys('shdid')
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[3]/dd/textarea').send_keys('shdid')
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/div/input[1]').click()

    def test_Message_sending_error1(self):
        "客户中心信息管理——信息发送内容不能少于5个字符！"
        # 登录

        time.sleep(1)
        self.driver.find_element(By.NAME, 'name').send_keys('admin')
        time.sleep(1)
        self.driver.find_element(By.NAME, 'password').send_keys('123456')
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()

        # 客户中心
        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[3]/a').click()
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[3]/ul/li[4]/a').click()
        time.sleep(1)
        # 信息发送
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div/a[3]').click()
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[1]/dd/input').send_keys('shdid')
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[3]/dd/textarea').send_keys('sh')
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/div/input[1]').click()
        self.assertEqual(self.driver.page_source.__contains__('内容不能少于5个字符！'), True)

    def test_Message_sending_error2(self):
        "客户中心信息管理——信息发送标题至少两个字符!"
        # 登录

        time.sleep(1)
        self.driver.find_element(By.NAME, 'name').send_keys('admin')
        time.sleep(1)
        self.driver.find_element(By.NAME, 'password').send_keys('123456')
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()

        # 客户中心
        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[3]/a').click()
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[3]/ul/li[4]/a').click()
        time.sleep(1)
        # 信息发送
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div/a[3]').click()
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[1]/dd/input').send_keys('d')
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[3]/dd/textarea').send_keys('sh购房人特')
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/div/input[1]').click()
        self.assertEqual(self.driver.page_source.__contains__('标题至少两个字符!'), True)

    def test_Message_delete_pass(self):
        "客户中心信息管理——信息删除"
        # 登录

        time.sleep(1)
        self.driver.find_element(By.NAME, 'name').send_keys('admin')
        time.sleep(1)
        self.driver.find_element(By.NAME, 'password').send_keys('123456')
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()

        # 客户中心
        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[3]/a').click()
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[3]/ul/li[4]/a').click()
        time.sleep(1)
        # 全选
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div/a[1]').click()
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div/a[1]').click()
        time.sleep(1)

        # 删除
        self.driver.find_element(By.XPATH,'//*[@id="content"]/form/table/tbody/tr[2]/td[1]/input').click()
        time.sleep(1)
        self.driver.find_element(By.XPATH,'//*[@id="content"]/form/div/a[2]').click()
        time.sleep(1)
        self.driver.find_element(By.XPATH, '/html/body/div[1]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[3]/td/div/button[1]').click()
        time.sleep(1)
        # 处理-删除
        from selenium.webdriver.common.action_chains import ActionChains
        el = self.driver.find_element(By.XPATH, '//*[@id="content"]/form/table/tbody/tr[2]/td[2]/div/a')
        ActionChains(self.driver).move_to_element(el).perform()
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/table/tbody/tr[2]/td[2]/div/div/ul/li/a').click()
        time.sleep(1)
        self.driver.find_element(By.XPATH, '/html/body/div[1]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[3]/td/div/button[1]').click()

if __name__ == '__main__':
    unittest.main()
