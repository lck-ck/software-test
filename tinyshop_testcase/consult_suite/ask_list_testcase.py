import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service


class ask_list_TestCase(unittest.TestCase):
    def setUp(self) -> None:
        driver_path = 'E:/PyCharm File/RuanJIanCeiShi/project/web_driver/chromedriver.exe'
        driver_server = Service(driver_path)
        self.driver = webdriver.Chrome(service=driver_server)
        self.driver.get('http://127.0.0.1/tinyshop/index.php?con=admin&act=login')
        self.driver.maximize_window()
        self.driver.implicitly_wait(10)

    def tearDown(self) -> None:
        time.sleep(3)
        self.driver.quit()

    def test_reply_pass(self):
        "回复客户中心商品咨询"
        # 登录

        time.sleep(1)
        self.driver.find_element(By.NAME, 'name').send_keys('admin')
        time.sleep(1)
        self.driver.find_element(By.NAME, 'password').send_keys('123456')
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()

        # 客户中心
        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[3]/a').click()
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[3]/ul/li[3]/a').click()
        time.sleep(1)


        # 处理-回复
        from selenium.webdriver.common.action_chains import ActionChains
        el = self.driver.find_element(By.XPATH, '//*[@id="content"]/form/table/tbody/tr[2]/td[2]/div/a')
        ActionChains(self.driver).move_to_element(el).perform()
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/table/tbody/tr[2]/td[2]/div/div/ul/li[1]/a').click()
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[4]/dd/textarea').send_keys('商品咨询ok')
        time.sleep(1)

        self.driver.find_element(By.XPATH,
                                 '//*[@id="obj_form"]/form/div/input[1] ').click()
        time.sleep(1)

    def test_reply_error(self):
        "回复客户中心商品咨询内容少于5个字符！"
        # 登录

        time.sleep(1)
        self.driver.find_element(By.NAME, 'name').send_keys('admin')
        time.sleep(1)
        self.driver.find_element(By.NAME, 'password').send_keys('123456')
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()

        # 客户中心
        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[3]/a').click()
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[3]/ul/li[3]/a').click()
        time.sleep(1)


        # 处理-回复
        from selenium.webdriver.common.action_chains import ActionChains
        el = self.driver.find_element(By.XPATH, '//*[@id="content"]/form/table/tbody/tr[2]/td[2]/div/a')
        ActionChains(self.driver).move_to_element(el).perform()
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/table/tbody/tr[2]/td[2]/div/div/ul/li[1]/a').click()
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[4]/dd/textarea').clear()
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/dl[4]/dd/textarea').send_keys('商品')
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="obj_form"]/form/div/input[1] ').click()

        self.assertEqual(self.driver.page_source.__contains__('内容不能少于5个字符！'), True)

    def test_ask_list_pass(self):
        "客户中心商品咨询筛选条件"
        # 登录
        time.sleep(1)
        self.driver.find_element(By.NAME, 'name').send_keys('admin')
        time.sleep(1)
        self.driver.find_element(By.NAME, 'password').send_keys('123456')
        time.sleep(1)
        self.driver.find_element(By.XPATH,'//*[@id="login"]/div/form/ul/li[4]/input[1]').click()

        # 客户中心
        self.driver.find_element(By.XPATH,'//*[@id="main_nav"]/li[3]/a').click()
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[3]/ul/li[3]/a').click()
        time.sleep(1)
        # 全选
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div/a[1]').click()
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div/a[1]').click()
        time.sleep(1)

        # 筛选条件
        self.driver.find_element(By.XPATH, '//*[@id="condition"]').click()
        time.sleep(1)
        # 添加筛选字段
        self.driver.find_element(By.XPATH, '//*[@id="condition_dialog"]/div[1]/span/select/option[2]').click()
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="condition_dialog"]/div[1]/span/select/option[4]').click()
        time.sleep(1)
        # 删除
        self.driver.find_element(By.XPATH, '//*[@id="condition_table"]/tbody/tr[2]/td[5]/a').click()
        time.sleep(1)
        # 添加筛选内容
        self.driver.find_element(By.XPATH, '//*[@id="condition_table"]/tbody/tr[2]/td[1]/select/option[2]').click()
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="condition_table"]/tbody/tr[2]/td[3]/select/option[7]').click()
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="condition_table"]/tbody/tr[2]/td[4]/select/option[1]').click()
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="condition_create"]').click()

    def test_ask_list_error(self):
        "客户中心商品咨询筛选条件【关系值】为空，筛选无效！"
        # 登录
        time.sleep(1)
        self.driver.find_element(By.NAME, 'name').send_keys('admin')
        time.sleep(1)
        self.driver.find_element(By.NAME, 'password').send_keys('123456')
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        # 客户中心
        self.driver.find_element(By.XPATH,'//*[@id="main_nav"]/li[3]/a').click()
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[3]/ul/li[3]/a').click()
        time.sleep(1)

        # 筛选条件
        self.driver.find_element(By.XPATH, '//*[@id="condition"]').click()
        time.sleep(1)
        # 添加筛选字段
        self.driver.find_element(By.XPATH, '//*[@id="condition_dialog"]/div[1]/span/select/option[2]').click()
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="condition_dialog"]/div[1]/span/select/option[4]').click()
        time.sleep(1)
        # 删除
        self.driver.find_element(By.XPATH, '//*[@id="condition_table"]/tbody/tr[3]/td[5]/a').click()
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="condition_create"]').click()

        self.assertEqual(self.driver.page_source.__contains__('所有【关系值】为空，筛选无效！'), True)

    def test_Delete_ask_list_pass(self):
        "删除客户中心商品咨询"
        # 登录

        time.sleep(1)
        self.driver.find_element(By.NAME, 'name').send_keys('admin')
        time.sleep(1)
        self.driver.find_element(By.NAME, 'password').send_keys('123456')
        time.sleep(1)
        self.driver.find_element(By.XPATH,'//*[@id="login"]/div/form/ul/li[4]/input[1]').click()

        # 客户中心
        self.driver.find_element(By.XPATH,'//*[@id="main_nav"]/li[3]/a').click()
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[3]/ul/li[3]/a').click()
        time.sleep(1)


        # 删除
        self.driver.find_element(By.XPATH,'//*[@id="content"]/form/table/tbody/tr[2]/td[1]/input').click()
        time.sleep(1)
        self.driver.find_element(By.XPATH,'//*[@id="content"]/form/div/a[2]').click()
        time.sleep(1)
        self.driver.find_element(By.XPATH, '/html/body/div[1]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[3]/td/div/button[1]').click()
        time.sleep(1)
        # 处理-删除
        from selenium.webdriver.common.action_chains import ActionChains
        el = self.driver.find_element(By.XPATH, '//*[@id="content"]/form/table/tbody/tr[2]/td[2]/div/a')
        ActionChains(self.driver).move_to_element(el).perform()
        time.sleep(1)
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/table/tbody/tr[2]/td[2]/div/div/ul/li[2]/a').click()
        time.sleep(1)
        self.driver.find_element(By.XPATH, '/html/body/div[1]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[3]/td/div/button[1]').click()

if __name__ == '__main__':
    unittest.main()
