"""
@Modify Time: 2022/11/27 21:29
@Author: LiuLei
Description
"""
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains


class Goods_classify_testCase(unittest.TestCase):
    def scroll(self, y):
        js = f'window.scrollBy(0,{y});'
        self.wd.execute_script(js)
        time.sleep(2)

    def s_by_xpath_click(self, ref):
        e = self.wd.find_element(By.XPATH, ref)
        e.click()
        return e

    def s_by_link_click(self, ref):
        e = self.wd.find_element(By.LINK_TEXT, ref)
        e.click()
        return e

    def s_by_name_input(self, ref, string='adsgjfgdsj'):
        e = self.wd.find_element(By.NAME, ref)
        e.send_keys(string)
        return e

    def setUp(self) -> None:
        self.wd = webdriver.Edge()
        self.ac = ActionChains(self.wd)
        url = 'http://127.0.0.1/tinyshop/index.php'
        self.wd.get(url)
        self.wd.implicitly_wait(10)
        self.wd.maximize_window()
        self.s_by_link_click('登录')
        self.s_by_name_input('account', 'test@qq.com')
        self.s_by_name_input('password', '123456')
        self.s_by_xpath_click('//*[@id="main"]/div/div/form/ul/li[4]/button')

    def tearDown(self) -> None:
        time.sleep(2)
        self.wd.quit()

    def test_classify_by_left_red_bar(self):
        '''通过页面的红色条分类商品'''
        self.ac.move_to_element(self.s_by_link_click('全部商品分类'))
        time.sleep(1)
        self.s_by_link_click('服饰')
        self.assertEqual(self.wd.page_source.__contains__('服饰'), True)

    def test_classify_by_left_red_bar_detail1(self):
        '''通过页面的红色条里面的二级菜单分类商品'''
        self.ac.move_to_element(self.s_by_link_click('全部商品分类'))
        time.sleep(1)
        self.ac.move_to_element(self.s_by_link_click('服饰'))
        time.sleep(1)
        self.s_by_link_click('女装')
        self.assertEqual(self.wd.page_source.__contains__('服饰'), True)

    def test_classify_by_left_red_bar_detail2(self):
        '''通过页面的红色条里面的三级菜单分类商品'''
        self.ac.move_to_element(self.s_by_link_click('全部商品分类'))
        time.sleep(1)
        self.ac.move_to_element(self.s_by_link_click('服饰'))
        time.sleep(1)
        self.s_by_link_click('衬衫')
        self.assertEqual(self.wd.page_source.__contains__('服饰'), True)

    def test_goods_screen(self):
        '''测试商品筛选功能'''
        self.ac.move_to_element(self.s_by_link_click('全部商品分类'))
        time.sleep(1)
        self.s_by_link_click('服饰')
        self.s_by_link_click('0-50')
        cur_url=self.wd.current_url
        flag = cur_url.__contains__('price=0-50')
        self.assertEqual(flag,True)

    def test_goods_display_by_certain_order(self):
        '''测试商品按特定顺序排序'''
        self.ac.move_to_element(self.s_by_link_click('全部商品分类'))
        time.sleep(1)
        self.s_by_link_click('服饰')
        self.s_by_link_click('销量')
        cur_url=self.wd.current_url
        flag = cur_url.__contains__('sort=1')
        self.assertEqual(flag,True)


if __name__ == '__main__':
    unittest.main()