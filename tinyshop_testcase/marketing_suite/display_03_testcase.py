"""
@Modify Time: 2022/11/27 22:23
@Author: LiuLei
Description
"""
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains

class Goods_detail_testCase(unittest.TestCase):
    def s_by_xpath_click(self, ref):
        e = self.wd.find_element(By.XPATH, ref)
        e.click()
        return e

    def s_by_link_click(self, ref):
        e = self.wd.find_element(By.LINK_TEXT, ref)
        e.click()
        return e

    def s_by_name_input(self, ref, string='adsgjfgdsj'):
        e = self.wd.find_element(By.NAME, ref)
        e.send_keys(string)
        return e

    def setUp(self) -> None:
        self.wd = webdriver.Edge()
        self.ac = ActionChains(self.wd)
        url = 'http://127.0.0.1/tinyshop/index.php'
        self.wd.get(url)
        self.wd.implicitly_wait(10)
        self.wd.maximize_window()
        self.s_by_link_click('登录')
        self.s_by_name_input('account', 'test@qq.com')
        self.s_by_name_input('password', '123456')
        self.s_by_xpath_click('//*[@id="main"]/div/div/form/ul/li[4]/button')

    def tearDown(self) -> None:
        time.sleep(2)
        self.wd.quit()

    def test_select_scale(self):
        '''测试选择商品规格'''
        self.s_by_xpath_click('//*[@id="main"]/div[3]/div[2]/div[2]/div[2]/ul/li[4]/dl/dd[1]/a')
        els = self.wd.find_elements(By.XPATH, '//*[@id="product-intro"]/div[3]/div/dl')
        for j in range(1,len(els)-2):
            self.s_by_xpath_click(f'//*[@id="product-intro"]/div[3]/div/dl[{j}]/dd/ul/li/span')
        classname=self.wd.find_element(By.XPATH,'//*[@id="product-intro"]/div[3]/div/dl[2]/dd/ul/li[1]').get_attribute('class')
        self.assertEqual(classname,'selected')

    def test_subscribe(self):
        '''测试关注商品'''
        self.s_by_xpath_click('//*[@id="main"]/div[3]/div[2]/div[2]/div[2]/ul/li[4]/dl/dd[1]/a')
        self.s_by_link_click('关注')
        self.assertEqual(self.wd.page_source.__contains__('成功关注了该商品'),True)

    def test_add_to_cart(self):
        '''加入购物车'''
        self.s_by_xpath_click('//*[@id="main"]/div[3]/div[2]/div[2]/div[2]/ul/li[4]/dl/dd[1]/a')
        els = self.wd.find_elements(By.XPATH, '//*[@id="product-intro"]/div[3]/div/dl')
        for j in range(1,len(els)-2):
            self.s_by_xpath_click(f'//*[@id="product-intro"]/div[3]/div/dl[{j}]/dd/ul/li/span')

        self.s_by_link_click('加入购物车')
        self.ac.move_to_element(self.s_by_xpath_click('//*[@id="shopping-cart"]/i'))
        time.sleep(2)
        flag=self.s_by_xpath_click('//*[@id="22"]/div[2]').is_displayed()
        self.assertEqual(flag, True)

    def test_goods_remark(self):
        '''查看商品评价'''
        self.s_by_xpath_click('//*[@id="main"]/div[3]/div[2]/div[2]/div[2]/ul/li[4]/dl/dd[1]/a')
        self.s_by_xpath_click('//*[@id="main"]/div[2]/div[2]/div[2]/div/div/div/ul/li[2]')
        self.assertEqual(self.wd.page_source.__contains__('参考评价'), True)

    def test_aftermarket(self):
        '''查看售后服务'''
        self.s_by_xpath_click('//*[@id="main"]/div[3]/div[2]/div[2]/div[2]/ul/li[4]/dl/dd[1]/a')
        self.s_by_xpath_click('//*[@id="main"]/div[2]/div[2]/div[2]/div/div/div/ul/li[4]')
        self.assertEqual(self.wd.page_source.__contains__('服务承诺'), True)

    def test_consultation(self):
        '''查看商品咨询'''
        self.s_by_xpath_click('//*[@id="main"]/div[3]/div[2]/div[2]/div[2]/ul/li[4]/dl/dd[1]/a')
        self.s_by_xpath_click('//*[@id="main"]/div[2]/div[2]/div[2]/div/div/div/ul/li[4]')
        self.assertEqual(self.wd.page_source.__contains__('商品咨询'), True)

if __name__ == '__main__':
    unittest.main()
