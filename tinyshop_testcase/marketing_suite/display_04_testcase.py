import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
class OthersTestcase(unittest.TestCase):
    def scroll(self, y):
        js = f'window.scrollBy(0,{y});'
        self.wd.execute_script(js)
        time.sleep(2)

    def s_by_xpath_click(self, ref):
        e = self.wd.find_element(By.XPATH, ref)
        e.click()
        return e

    def s_by_link_click(self, ref):
        e = self.wd.find_element(By.LINK_TEXT, ref)
        e.click()
        return e

    def s_by_name_input(self, ref, string='adsgjfgdsj'):
        e = self.wd.find_element(By.NAME, ref)
        e.send_keys(string)
        return e

    def setUp(self) -> None:
        self.wd = webdriver.Edge()
        self.ac = ActionChains(self.wd)
        url = 'http://127.0.0.1/tinyshop/index.php'
        self.wd.get(url)
        self.wd.implicitly_wait(10)
        self.wd.maximize_window()
        self.s_by_link_click('登录')
        self.s_by_name_input('account', 'test@qq.com')
        self.s_by_name_input('password', '123456')
        self.s_by_xpath_click('//*[@id="main"]/div/div/form/ul/li[4]/button')
        self.s_by_xpath_click('//*[@id="main"]/div[3]/div[1]/div[2]/div[2]/ul/li[1]/dl/dd[1]/a')

    def tearDown(self) -> None:
        time.sleep(2)
        self.wd.quit()

    def test_remark(self):
        '''评论购买过的商品'''
        self.s_by_xpath_click('//*[@id="main"]/div[2]/div[2]/div[2]/div/div/div[2]/ul/li[2]')
        self.s_by_link_click('我要评论')
        self.s_by_xpath_click('//*[@id="rate-comment"]/ul')
        self.s_by_name_input('content')
        self.s_by_xpath_click('//*[@id="main"]/div/div/div[2]/div/form/table/tbody/tr[3]/td/input')
        contains__ = self.wd.page_source.__contains__('评论成功')
        self.assertEqual(contains__,True)

    def test_remark1(self):
        '''评论购买过的商品时不选择商品满意度'''
        self.s_by_xpath_click('//*[@id="main"]/div[2]/div[2]/div[2]/div/div/div[2]/ul/li[2]')
        self.s_by_link_click('我要评论')
        self.s_by_xpath_click('//*[@id="rate-comment"]/ul')
        self.s_by_name_input('content')
        self.s_by_xpath_click('//*[@id="main"]/div/div/div[2]/div/form/table/tbody/tr[3]/td/input')
        contains__ = self.wd.page_source.__contains__('评打分后才能提交！')
        self.assertEqual(contains__,True)


    def test_remark2(self):
        '''评论购买过的商品时不输入评价'''
        self.s_by_xpath_click('//*[@id="main"]/div[2]/div[2]/div[2]/div/div/div[2]/ul/li[2]')
        self.s_by_link_click('我要评论')
        self.s_by_xpath_click('//*[@id="rate-comment"]/ul')
        self.s_by_xpath_click('//*[@id="main"]/div/div/div[2]/div/form/table/tbody/tr[3]/td/input')
        contains__ = self.wd.page_source.__contains__('评论成功')
        self.assertEqual(contains__,True)

    def test_remark3(self):
        '''查看很好的评论'''
        self.s_by_xpath_click('//*[@id="main"]/div[2]/div[2]/div[2]/div/div/div[2]/ul/li[2]')
        self.s_by_xpath_click('//*[@id="comment"]/ul/li[2]')

    def test_remark4(self):
        '''查看较好好的评论'''
        self.s_by_xpath_click('//*[@id="main"]/div[2]/div[2]/div[2]/div/div/div[2]/ul/li[2]')
        self.s_by_xpath_click('//*[@id="comment"]/ul/li[3]')

    def test_remark5(self):
        '''查看一般的评论'''
        self.s_by_xpath_click('//*[@id="main"]/div[2]/div[2]/div[2]/div/div/div[2]/ul/li[2]')
        self.s_by_xpath_click('//*[@id="comment"]/ul/li[4]')


if __name__ == '__main__':
    unittest.main()