
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains


class EachDetailPageTestcase(unittest.TestCase):
    def scroll(self, y):
        js = f'window.scrollBy(0,{y});'
        self.wd.execute_script(js)
        time.sleep(2)

    def s_by_xpath_click(self, ref):
        e = self.wd.find_element(By.XPATH, ref)
        e.click()
        return e

    def s_by_link_click(self, ref):
        e = self.wd.find_element(By.LINK_TEXT, ref)
        e.click()
        return e

    def s_by_name_input(self, ref, string='adsgjfgdsj'):
        e = self.wd.find_element(By.NAME, ref)
        e.send_keys(string)
        return e

    def setUp(self) -> None:
        self.wd = webdriver.Edge()
        self.ac = ActionChains(self.wd)
        url = 'http://127.0.0.1/tinyshop/index.php'
        self.wd.get(url)
        self.wd.implicitly_wait(10)
        self.wd.maximize_window()
        self.s_by_link_click('登录')
        self.s_by_name_input('account', 'test007@qq.com')
        self.s_by_name_input('password', '123456')
        self.s_by_xpath_click('//*[@id="main"]/div/div/form/ul/li[4]/button')

    def tearDown(self) -> None:
        time.sleep(2)
        self.wd.quit()

    def test_look_up_flash(self):
        '''访问限时限时抢购'''
        self.s_by_link_click('限时抢购')
        flag=self.wd.title.__contains__('抢购,优惠精选-TinyShop大型电子商务系统')
        self.assertEqual(flag,True)

    def test_look_up_group(self):
        '''访问限时团购'''
        self.s_by_link_click('团购')
        flag=self.wd.title.__contains__('团购,优惠精选-TinyShop大型电子商务系统')
        self.assertEqual(flag,True)

    def test_look_up_clothes(self):
        '''访问服装页面'''
        self.s_by_link_click('服装')
        flag=self.wd.title.__contains__('服饰,优惠精选-TinyShop大型电子商务系统')
        self.assertEqual(flag,True)

    def test_look_up_clothes(self):
        '''访问手机商城'''
        self.s_by_link_click('手机商城')
        flag=self.wd.title.__contains__('手机,优惠精选-TinyShop大型电子商务系统')
        self.assertEqual(flag,True)


    def test_look_up_detail_page(self):
        '''访问商品详情页'''
        self.s_by_xpath_click('//*[@id="main"]/div[3]/div[2]/div[2]/div[2]/ul/li[4]/dl/dd[1]/a')
        flag=self.wd.page_source.__contains__('商品详情')
        self.assertEqual(flag,True)


if __name__ == '__main__':
    unittest.main()
