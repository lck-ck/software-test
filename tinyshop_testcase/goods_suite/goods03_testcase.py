"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/17 9:27
# @Author : LuChangKai
# @File : order01_testcase
# @Project : Utest1
对后台商品中心的品牌规格进行测试
"""
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.select import Select
# 前台登录的测试用例
import unittest


class goods_03_Testcase(unittest.TestCase):
    def setUp(self) -> None:
        # self.driver_path='E:/python_workspace/Utest1/web_driver/chromedriver.exe'
        # self.driver_server=Service(self.driver_path)

        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(10)

        self.driver.get('http://192.168.217.133:81/tinyshop/index.php?con=admin&act=login')

        self.driver.maximize_window()

    def tearDown(self) -> None:
        self.driver.quit()

    # 1.通过测试用例
    def test_goods_pass(self):
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('admin123')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()

        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[1]/a').click()
        # 点击商品管理
        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[2]/ul/li[5]/a').click()
        # 点击添加
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div/a[3]').click()
        # 填写品牌名称
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/div[1]/div[1]/dl[1]/dd/input').send_keys('测试手机')
         # 填写网址
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/div[1]/div[1]/dl[2]/dd/input').send_keys('http://www.huawei.com')
        # 填写排序
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/div[1]/div[1]/dl[3]/dd/input').send_keys('5')
        # 选择图片
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/div[1]/div[1]/dl[4]/dd/button').click()
        # 切换框架
        frame=self.driver.find_element(By.XPATH,'/html/body/div[1]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[2]/td[2]/div/iframe')

        self.driver.switch_to.frame(frame)
        # 点击图库图片
        self.driver.find_element(By.XPATH,'/html/body/div[2]/ul/li[2]').click()
        # 选择图片
        self.driver.find_element(By.XPATH,'/html/body/div[2]/div/div[2]/ul/li[2]/img').click()
        # 点击保存
        self.driver.find_element(By.XPATH,'/html/body/div[3]/button').click()
        # 点击提交
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/div[2]/input[1]').click()

        # 等于断言
        self.assertEqual(self.driver.page_source.__contains__('测试手机'), True)

        # 2.url错误
    def test1_goods_error_url(self):
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('admin123')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()

        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[1]/a').click()
        # 点击商品管理
        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[2]/ul/li[5]/a').click()
        # 点击添加
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div/a[3]').click()
        # 填写品牌名称
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/div[1]/div[1]/dl[1]/dd/input').send_keys('测试手机')
         # 填写网址
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/div[1]/div[1]/dl[2]/dd/input').send_keys('//www.huawei.com')
        # 填写排序
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/div[1]/div[1]/dl[3]/dd/input').send_keys('5')
        # 选择图片
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/div[1]/div[1]/dl[4]/dd/button').click()
        # 切换框架
        frame=self.driver.find_element(By.XPATH,'/html/body/div[1]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[2]/td[2]/div/iframe')

        self.driver.switch_to.frame(frame)
        # 点击图库图片
        self.driver.find_element(By.XPATH,'/html/body/div[2]/ul/li[2]').click()
        # 选择图片
        self.driver.find_element(By.XPATH,'/html/body/div[2]/div/div[2]/ul/li[2]/img').click()
        # 点击保存
        self.driver.find_element(By.XPATH,'/html/body/div[3]/button').click()
        # 点击提交
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/div[2]/input[1]').click()

        # 等于断言
        self.assertEqual(self.driver.page_source.__contains__('Url地址格式错误'), True)
 # 2.名称不能为空
    def test2_goods_name_isNull(self):
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('admin123')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()

        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[1]/a').click()
        # 点击商品管理
        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[2]/ul/li[5]/a').click()
        # 点击添加
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div/a[3]').click()
        # 填写品牌名称
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/div[1]/div[1]/dl[1]/dd/input').send_keys('')
         # 填写网址
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/div[1]/div[1]/dl[2]/dd/input').send_keys('//www.huawei.com')
        # 填写排序
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/div[1]/div[1]/dl[3]/dd/input').send_keys('5')
        # 选择图片
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/div[1]/div[1]/dl[4]/dd/button').click()
        # 切换框架
        frame=self.driver.find_element(By.XPATH,'/html/body/div[1]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[2]/td[2]/div/iframe')

        self.driver.switch_to.frame(frame)
        # 点击图库图片
        self.driver.find_element(By.XPATH,'/html/body/div[2]/ul/li[2]').click()
        # 选择图片
        self.driver.find_element(By.XPATH,'/html/body/div[2]/div/div[2]/ul/li[2]/img').click()
        # 点击保存
        self.driver.find_element(By.XPATH,'/html/body/div[3]/button').click()
        # 点击提交
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/div[2]/input[1]').click()

        # 等于断言
        self.assertEqual(self.driver.page_source.__contains__('名称不能为空'), True)
        # 2.排序字符不合法
    def test3_goods_illegal_sortchar(self):
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('admin123')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()

        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[1]/a').click()
        # 点击商品管理
        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[2]/ul/li[5]/a').click()
        # 点击添加
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div/a[3]').click()
        # 填写品牌名称
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/div[1]/div[1]/dl[1]/dd/input').send_keys('')
         # 填写网址
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/div[1]/div[1]/dl[2]/dd/input').send_keys('//www.huawei.com')
        # 填写排序
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/div[1]/div[1]/dl[3]/dd/input').send_keys('aaaaaa')
        # 选择图片
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/div[1]/div[1]/dl[4]/dd/button').click()
        # 切换框架
        frame=self.driver.find_element(By.XPATH,'/html/body/div[1]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[2]/td[2]/div/iframe')

        self.driver.switch_to.frame(frame)
        # 点击图库图片
        self.driver.find_element(By.XPATH,'/html/body/div[2]/ul/li[2]').click()
        # 选择图片
        self.driver.find_element(By.XPATH,'/html/body/div[2]/div/div[2]/ul/li[2]/img').click()
        # 点击保存
        self.driver.find_element(By.XPATH,'/html/body/div[3]/button').click()
        # 点击提交
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/div[2]/input[1]').click()

        # 等于断言
        self.assertEqual(self.driver.page_source.__contains__('必需为数字'), True)
if __name__ == '__main__':
    unittest.main()
