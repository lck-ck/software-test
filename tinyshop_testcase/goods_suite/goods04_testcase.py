"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/17 9:27
# @Author : LuChangKai
# @File : order01_testcase
# @Project : Utest1
后台商品中心-规格管理
"""
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.select import Select
# 前台登录的测试用例
import unittest


class goods_04_Testcase(unittest.TestCase):
    def setUp(self) -> None:
        # self.driver_path='E:/python_workspace/Utest1/web_driver/chromedriver.exe'
        # self.driver_server=Service(self.driver_path)

        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(10)

        self.driver.get('http://192.168.217.133:81/tinyshop/index.php?con=admin&act=login')

        self.driver.maximize_window()

    def tearDown(self) -> None:
        self.driver.quit()

    # 1.通过测试用例
    def test_goods_pass(self):
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('admin123')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()

        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[1]/a').click()
        # 点击规格管理
        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[2]/ul/li[4]/a').click()
        # 点击添加
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div/a[3]').click()
        # 填写名称
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/dl[1]/dd/input').send_keys('颜色')
         # 填写备注
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/dl[3]/dd/input').send_keys('颜色样式')
        # 规格值名称
        self.driver.find_element(By.XPATH,'//*[@id="spec"]/tbody/tr[2]/td[1]/input[2]').send_keys('红色')


        # 点击提交
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/div[2]/input[1]').click()

        # 等于断言
        self.assertEqual(self.driver.page_source.__contains__('颜色样式'), True)
# 2.名称为空
    def test1_goods_name_isNull(self):
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('admin123')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()

        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[1]/a').click()
        # 点击规格管理
        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[2]/ul/li[4]/a').click()
        # 点击添加
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div/a[3]').click()
        # 填写名称
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/dl[1]/dd/input').send_keys('')
         # 填写备注
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/dl[3]/dd/input').send_keys('颜色样式')
        # 规格值名称
        self.driver.find_element(By.XPATH,'//*[@id="spec"]/tbody/tr[2]/td[1]/input[2]').send_keys('红色')


        # 点击提交
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/div[2]/input[1]').click()

        # 等于断言
        self.assertEqual(self.driver.page_source.__contains__('名称不能为空'), True)
        # 3.注释为空
    def test2_goods_remark_isNull(self):
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('admin123')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()

        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[1]/a').click()
        # 点击规格管理
        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[2]/ul/li[4]/a').click()
        # 点击添加
        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/div/a[3]').click()
        # 填写名称
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/dl[1]/dd/input').send_keys('aaaa')
         # 填写备注
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/dl[3]/dd/input').send_keys('')
        # 规格值名称
        self.driver.find_element(By.XPATH,'//*[@id="spec"]/tbody/tr[2]/td[1]/input[2]').send_keys('红色')


        # 点击提交
        self.driver.find_element(By.XPATH,'//*[@id="obj_form"]/form/div[2]/input[1]').click()

        # 等于断言
        self.assertEqual(self.driver.page_source.__contains__(''), True)


if __name__ == '__main__':
    unittest.main()
