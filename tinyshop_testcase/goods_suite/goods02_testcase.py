"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/17 9:27
# @Author : LuChangKai
# @File : order01_testcase
# @Project : Utest1
后台商品中心-商品管理，对商品进行上下架操作
"""
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.select import Select
# 前台登录的测试用例
import unittest


class goods_01_Testcase(unittest.TestCase):
    def setUp(self) -> None:
        # self.driver_path='E:/python_workspace/Utest1/web_driver/chromedriver.exe'
        # self.driver_server=Service(self.driver_path)

        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(10)

        self.driver.get('http://192.168.217.133:81/tinyshop/index.php?con=admin&act=login')

        self.driver.maximize_window()

    def tearDown(self) -> None:
        self.driver.quit()

    # 1.通过测试用例，对商品进行下架
    def test_goods_pass(self):
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('admin123')

        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()

        self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[1]/a').click()
        # 点击商品管理
        self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[1]/ul/li[2]/a').click()



        # 鼠标悬停
        el = self.driver.find_element(By.XPATH, '/html/body/div[3]/div[2]/form/table/tbody/tr[2]/td[2]/div/a')

        ActionChains(self.driver).move_to_element(el).perform()

        self.driver.find_element(By.XPATH, '//*[@id="content"]/form/table/tbody/tr[2]/td[2]/div/div/ul/li[2]/a').click()

        # 等于断言
        self.assertEqual(self.driver.page_source.__contains__('下架'), True)

        # 2.通过测试用例，对商品进行下架
    def test1_goods_pass(self):
            self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')

            self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('admin123')

            self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()

            self.driver.find_element(By.XPATH, '//*[@id="main_nav"]/li[1]/a').click()
            # 点击商品管理
            self.driver.find_element(By.XPATH, '//*[@id="sidebar"]/ul/li[1]/ul/li[2]/a').click()

            # 鼠标悬停
            el = self.driver.find_element(By.XPATH, '/html/body/div[3]/div[2]/form/table/tbody/tr[2]/td[2]/div/a')

            ActionChains(self.driver).move_to_element(el).perform()

            self.driver.find_element(By.XPATH,
                                     '//*[@id="content"]/form/table/tbody/tr[2]/td[2]/div/div/ul/li[1]/a').click()

            # 等于断言
            self.assertEqual(self.driver.page_source.__contains__('在售'), True)

if __name__ == '__main__':
    unittest.main()
