#后台登录
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By

class Login_05_testcase(unittest.TestCase):

    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        url = 'http://127.0.0.1/tinyshop/index.php?con=admin&act=login'
        self.driver.get(url)
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()


    def tearDown(self) -> None:
        time.sleep(1)
        self.driver.quit()


    #通过测试
    def test_login_pass(self):
        '''通过测试'''
        self.driver.find_element(By.XPATH,'//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')
        self.driver.find_element(By.XPATH,'//*[@id="login"]/div/form/ul/li[2]/input').send_keys('123456')
        self.driver.find_element(By.XPATH,'//*[@id="login"]/div/form/ul/li[4]/input[1]').click()

    #不填写用户名
    def test_no_name(self):
        # time.sleep(1)
        # self.driver.find_element(By.XPATH,'//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')
        '''不填写用户名'''
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('test007@qq.com')
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        self.assertEqual(self.driver.page_source.__contains__('用户名或者密码错误'),True)

    #没有密码
    def test_no_password(self):
        '''密码错误'''
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[1]/input').send_keys('admin')
        time.sleep(1)
        # self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[2]/input').send_keys('123456')
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        self.assertEqual(self.driver.page_source.__contains__('用户名或者密码错误'), True)

    def test_no_usernameAndpassword(self):
        '''用户名或者密码错误'''
        self.driver.find_element(By.XPATH, '//*[@id="login"]/div/form/ul/li[4]/input[1]').click()
        self.assertEqual(self.driver.page_source.__contains__('用户名或者密码错误'), True)

if __name__ == '__main__':
        unittest.main()



