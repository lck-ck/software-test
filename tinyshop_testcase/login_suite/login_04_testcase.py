#前台注册
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By

class Login_04_testcase(unittest.TestCase):

    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        url = 'http://127.0.0.1/tinyshop/'
        self.driver.get(url)
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        self.driver.find_element(By.XPATH,'//*[@id="header"]/div[1]/div/div[2]/ul/li[7]/a[2]').click()

    def tearDown(self) -> None:
        time.sleep(1)
        self.driver.quit()


    def test_registered_pass(self):
        '''注册通过'''
        self.driver.find_element(By.XPATH,'//*[@id="email"]').send_keys(f'{time.time()}@qq.com')
        password='time.time()'
        self.driver.find_element(By.XPATH,'//*[@id="main"]/div[1]/div/form/ul/li[2]/input').send_keys(password)
        self.driver.find_element(By.XPATH,'//*[@id="main"]/div[1]/div/form/ul/li[3]/input').send_keys(password)
        self.driver.find_element(By.XPATH,'//*[@id="readme"]').click()
        # self.driver.find_element(By.XPATH,'//*[@id="main"]/div[1]/div/form/ul/li[6]/button').click()

    def test_no_username(self):
        '''没有填写用户名'''
        # self.driver.find_element(By.XPATH, '//*[@id="email"]').send_keys(f'{time.time()}@qq.com')
        password = 'time.time()'
        self.driver.find_element(By.XPATH, '//*[@id="main"]/div[1]/div/form/ul/li[2]/input').send_keys(password)
        self.driver.find_element(By.XPATH, '//*[@id="main"]/div[1]/div/form/ul/li[3]/input').send_keys(password)
        self.driver.find_element(By.XPATH, '//*[@id="readme"]').click()
        self.driver.find_element(By.XPATH, '//*[@id="main"]/div[1]/div/form/ul/li[6]/button').click()
        self.assertEqual(self.driver.page_source.__contains__('邮箱格式不正确!'), True)

    def test_no_password(self):
        '''没有填写密码'''
        self.driver.find_element(By.XPATH, '//*[@id="email"]').send_keys(f'{time.time()}@qq.com')
        password = 'time.time()'
        # self.driver.find_element(By.XPATH, '//*[@id="main"]/div[1]/div/form/ul/li[2]/input').send_keys(password)
        self.driver.find_element(By.XPATH, '//*[@id="main"]/div[1]/div/form/ul/li[3]/input').send_keys(password)
        self.driver.find_element(By.XPATH, '//*[@id="readme"]').click()
        self.driver.find_element(By.XPATH, '//*[@id="main"]/div[1]/div/form/ul/li[6]/button').click()
        self.assertEqual(self.driver.page_source.__contains__('6-20任意字符组合'), True)

    def test_no_repeatPassword(self):
        '''确认密码没有填写'''
        self.driver.find_element(By.XPATH, '//*[@id="email"]').send_keys(f'{time.time()}@qq.com')
        password = 'time.time()'
        self.driver.find_element(By.XPATH, '//*[@id="main"]/div[1]/div/form/ul/li[2]/input').send_keys()
        self.driver.find_element(By.XPATH, '//*[@id="main"]/div[1]/div/form/ul/li[3]/input').send_keys(password)
        self.driver.find_element(By.XPATH, '//*[@id="readme"]').click()
        self.driver.find_element(By.XPATH,'//*[@id="main"]/div[1]/div/form/ul/li[6]/button').click()
        self.assertEqual(self.driver.page_source.__contains__('6-20任意字符组合'), True)

    def test_Verification_failed(self):
        '''验证码错误或没有填写'''
        self.driver.find_element(By.XPATH,'//*[@id="email"]').send_keys(f'{time.time()}@qq.com')
        password='time.time()'
        self.driver.find_element(By.XPATH,'//*[@id="main"]/div[1]/div/form/ul/li[2]/input').send_keys(password)
        self.driver.find_element(By.XPATH,'//*[@id="main"]/div[1]/div/form/ul/li[3]/input').send_keys(password)
        self.driver.find_element(By.XPATH,'//*[@id="readme"]').click()
        self.driver.find_element(By.XPATH,'//*[@id="main"]/div[1]/div/form/ul/li[6]/button').click()
        self.assertEqual(self.driver.page_source.__contains__('验证码不正确'), True)

    def test_passwordDifferent(self):
        '''俩次输入密码不一致'''
        self.driver.find_element(By.XPATH, '//*[@id="email"]').send_keys(f'{time.time()}@qq.com')
        self.driver.find_element(By.XPATH, '//*[@id="main"]/div[1]/div/form/ul/li[2]/input').send_keys(time.time())
        self.driver.find_element(By.XPATH, '//*[@id="main"]/div[1]/div/form/ul/li[3]/input').send_keys(time.time())
        self.driver.find_element(By.XPATH, '//*[@id="readme"]').click()
        self.driver.find_element(By.XPATH, '//*[@id="main"]/div[1]/div/form/ul/li[6]/button').click()
        self.assertEqual(self.driver.page_source.__contains__('两次输入密码不一致'), True)


if __name__ == '__main__':
        unittest.main()


