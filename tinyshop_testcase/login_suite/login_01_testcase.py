"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/14 17:07
# @Author : LuChangKai
# @File : login_testcase01
# @Project : Utest1
"""
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service

# 前台登录的测试用例
import unittest
class Login_01_Testcase(unittest.TestCase):
    def setUp(self) -> None:
        # self.driver_path='E:/python_workspace/Utest1/web_driver/chromedriver.exe'
        # self.driver_server=Service(self.driver_path)

        self.driver=webdriver.Chrome()
        self.driver.implicitly_wait(10)

        self.driver.get('http://192.168.217.133:81/tinyshop/')

        self.driver.maximize_window()


    def tearDown(self) -> None:

        self.driver.quit()
    # 1.通过测试用例
    def test_login_pass(self):
        '''前台通过测试用例'''
        self.driver.find_element(By.LINK_TEXT,'登录').click()

        self.driver.find_element(By.NAME,'account').send_keys('test@qq.com')

        self.driver.find_element(By.NAME,'password').send_keys('admin123')
        self.driver.find_element(By.XPATH,'//*[@id="main"]/div/div/form/ul/li[4]/button').click()
        # 等于断言
        self.assertEqual(self.driver.page_source.__contains__('你好:test@qq.com'),True)

    # 2.登录用户名错误
    def test_username_error(self):
        '''前台客户端登录用户名错误'''
        self.driver.find_element(By.LINK_TEXT, '登录').click()

        self.driver.find_element(By.NAME, 'account').send_keys('test@qq.co')

        self.driver.find_element(By.NAME, 'password').send_keys('admin123')
        self.driver.find_element(By.XPATH,'//*[@id="main"]/div/div/form/ul/li[4]/button').click()
        # 等于断言
        self.assertEqual(self.driver.page_source.__contains__('用户不存在'), True)
    #3.登录的用户名为空
    def test_username_is_null(self):
        '''前台登录用户名为空'''
        self.driver.find_element(By.LINK_TEXT, '登录').click()

        self.driver.find_element(By.NAME, 'account').send_keys('')

        self.driver.find_element(By.NAME, 'password').send_keys('admin123')
        self.driver.find_element(By.XPATH,'//*[@id="main"]/div/div/form/ul/li[4]/button').click()
        # 等于断言
        self.assertEqual(self.driver.page_source.__contains__('用户名为空'), True)
    # 4.密码错误
    def test_password_error(self):
        '''前台登录密码错误'''
        self.driver.find_element(By.LINK_TEXT, '登录').click()

        self.driver.find_element(By.NAME, 'account').send_keys('test@qq.com')

        self.driver.find_element(By.NAME, 'password').send_keys('admin1')
        self.driver.find_element(By.XPATH,'//*[@id="main"]/div/div/form/ul/li[4]/button').click()
        # 等于断言
        self.assertEqual(self.driver.page_source.__contains__('密码错误'), True)
    # 5.登录的密码为空
    def test_password_is_null(self):
        '''密码为空'''
        self.driver.find_element(By.LINK_TEXT, '登录').click()

        self.driver.find_element(By.NAME, 'account').send_keys('test@qq.com')

        self.driver.find_element(By.NAME, 'password').send_keys('')
        self.driver.find_element(By.XPATH,'//*[@id="main"]/div/div/form/ul/li[4]/button').click()
        # 等于断言
        self.assertEqual(self.driver.page_source.__contains__('密码不能为空'), True)

if __name__ == '__main__':
    unittest.main()