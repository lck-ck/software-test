
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains

class ModifyAddrTestCase(unittest.TestCase):
    def s_by_xpath_click(self, ref):
        e = self.wd.find_element(By.XPATH, ref)
        e.click()
        return e

    def s_by_link_click(self, ref):
        e = self.wd.find_element(By.LINK_TEXT, ref)
        e.click()
        return e

    def s_by_name_input(self, ref, string='adsgjfgdsj'):
        e = self.wd.find_element(By.NAME, ref)
        e.clear()
        e.send_keys(string)
        return e

    def setUp(self) -> None:
        self.wd=webdriver.Chrome()
        self.ac = ActionChains(self.wd)
        url = 'http://127.0.0.1/tinyshop/index.php'
        self.wd.get(url)
        self.wd.implicitly_wait(10)
        self.wd.maximize_window()
        self.s_by_link_click('登录')
        self.s_by_name_input('account', 'test@qq.com')
        self.s_by_name_input('password', '123456')
        self.s_by_xpath_click('//*[@id="main"]/div/div/form/ul/li[4]/button')
        #测试商品至购物车
        self.s_by_xpath_click('//*[@id="main"]/div[3]/div[2]/div[2]/div[2]/ul/li[4]/dl/dd[1]/a')
        els = self.wd.find_elements(By.XPATH, '//*[@id="product-intro"]/div[3]/div/dl')
        for j in range(1,len(els)-2):
            self.s_by_xpath_click(f'//*[@id="product-intro"]/div[3]/div/dl[{j}]/dd/ul/li/span')

        self.s_by_link_click('加入购物车')
        self.ac.move_to_element(self.s_by_xpath_click('//*[@id="shopping-cart"]/i'))
        self.s_by_xpath_click('//*[@id="shopping-cart"]/div/div/a')
        self.s_by_link_click('立即结算')


    def tearDown(self) -> None:
        time.sleep(2)
        self.wd.quit()

    def test_modify_address(self):
        '''购物车确认订单信息时修改地址'''
        self.s_by_link_click('修改地址')
        self.wd.switch_to.frame(self.s_by_xpath_click('/html/body/div[1]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[2]/td[2]/div/iframe'))
        self.s_by_xpath_click('//*[@id="province"]/option[3]')
        self.s_by_xpath_click('//*[@id="city"]/option[2]')
        self.s_by_xpath_click('//*[@id="county"]/option[4]')
        self.s_by_name_input('zip',411302)
        self.s_by_name_input('addr')
        self.s_by_name_input('accept_name')
        self.s_by_name_input('mobile',17726130771)
        self.s_by_name_input('phone',17726130771)
        self.s_by_xpath_click('//*[@id="address-form"]/table/tbody/tr[8]/td/input')
        self.wd.switch_to.default_content()

    def test_modify_address1(self):
        '''购物车确认订单信息修改地址时不填所在地区'''
        self.s_by_link_click('修改地址')
        self.wd.switch_to.frame(self.s_by_xpath_click('/html/body/div[1]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[2]/td[2]/div/iframe'))
        self.s_by_xpath_click('//*[@id="province"]/option[1]')
        self.s_by_xpath_click('//*[@id="city"]/option[1]')
        self.s_by_xpath_click('//*[@id="county"]/option[1]')
        self.s_by_name_input('zip',411302)
        self.s_by_name_input('addr')
        self.s_by_name_input('accept_name')
        self.s_by_name_input('mobile',17726130771)
        self.s_by_name_input('phone',17726130771)
        self.s_by_xpath_click('//*[@id="address-form"]/table/tbody/tr[8]/td/input')

    def test_modify_address2(self):
        '''购物车确认订单信息修改地址时只填部分所在地区'''
        self.s_by_link_click('修改地址')
        self.wd.switch_to.frame(self.s_by_xpath_click('/html/body/div[1]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[2]/td[2]/div/iframe'))
        self.s_by_xpath_click('//*[@id="province"]/option[1]')
        self.s_by_name_input('zip',411302)
        self.s_by_name_input('addr')
        self.s_by_name_input('accept_name')
        self.s_by_name_input('mobile',17726130771)
        self.s_by_name_input('phone',17726130771)
        self.s_by_xpath_click('//*[@id="address-form"]/table/tbody/tr[8]/td/input')
        self.assertEqual(self.wd.page_source.__contains__('请选择完整地区信息'),True)

    # def test_modify_address(self):
    #     '''购物车确认订单信息时修改地址不添加邮政编码'''
    #     self.s_by_link_click('修改地址')
    #     self.wd.switch_to.frame(self.s_by_xpath_click('/html/body/div[1]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[2]/td[2]/div/iframe'))
    #     self.s_by_xpath_click('//*[@id="province"]/option[3]')
    #     self.s_by_xpath_click('//*[@id="city"]/option[2]')
    #     self.s_by_xpath_click('//*[@id="county"]/option[4]')
    #     self.s_by_name_input('addr')
    #     self.s_by_name_input('accept_name')
    #     self.s_by_name_input('mobile',17726130771)
    #     self.s_by_name_input('phone',17726130771)
    #     self.s_by_xpath_click('//*[@id="address-form"]/table/tbody/tr[8]/td/input')
    #     self.assertEqual(self.wd.page_source.__contains__('请选择完整地区信息'), True)

    def test_modify_address3(self):
        '''购物车确认订单信息时修改地址不添加邮政编码'''
        self.s_by_link_click('修改地址')
        self.wd.switch_to.frame(self.s_by_xpath_click('/html/body/div[1]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[2]/td[2]/div/iframe'))
        self.s_by_xpath_click('//*[@id="province"]/option[3]')
        self.s_by_xpath_click('//*[@id="city"]/option[2]')
        self.s_by_xpath_click('//*[@id="county"]/option[4]')
        self.s_by_name_input('zip','')
        self.s_by_name_input('addr')
        self.s_by_name_input('accept_name')
        self.s_by_name_input('mobile',17726130771)
        self.s_by_name_input('phone',17726130771)
        self.s_by_xpath_click('//*[@id="address-form"]/table/tbody/tr[8]/td/input')
        self.assertEqual(self.wd.page_source.__contains__('邮政编码错误'), True)

    def test_modify_address4(self):
        '''购物车确认订单信息时修改地址添加邮政编码时不按格式'''
        self.s_by_link_click('修改地址')
        self.wd.switch_to.frame(self.s_by_xpath_click('/html/body/div[1]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[2]/td[2]/div/iframe'))
        self.s_by_xpath_click('//*[@id="province"]/option[3]')
        self.s_by_xpath_click('//*[@id="city"]/option[2]')
        self.s_by_xpath_click('//*[@id="county"]/option[4]')
        self.s_by_name_input('zip')
        self.s_by_name_input('addr')
        self.s_by_name_input('accept_name')
        self.s_by_name_input('mobile',17726130771)
        self.s_by_name_input('phone',17726130771)
        self.s_by_xpath_click('//*[@id="address-form"]/table/tbody/tr[8]/td/input')
        self.assertEqual(self.wd.page_source.__contains__('邮政编码错误'), True)

    def test_modify_address5(self):
        '''购物车确认订单信息时修改地址不添加街道信息'''
        self.s_by_link_click('修改地址')
        self.wd.switch_to.frame(self.s_by_xpath_click(
            '/html/body/div[1]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[2]/td[2]/div/iframe'))
        self.s_by_xpath_click('//*[@id="province"]/option[1]')
        self.s_by_xpath_click('//*[@id="city"]/option[1]')
        self.s_by_xpath_click('//*[@id="county"]/option[1]')
        self.s_by_name_input('zip', 411302)
        self.s_by_name_input('addr','')
        self.s_by_name_input('accept_name')
        self.s_by_name_input('mobile', 17726130771)
        self.s_by_name_input('phone', 17726130771)
        self.s_by_xpath_click('//*[@id="address-form"]/table/tbody/tr[8]/td/input')


    def test_modify_address6(self):
        '''购物车确认订单信息时修改地址不添加收信人姓名'''
        self.s_by_link_click('修改地址')
        self.wd.switch_to.frame(self.s_by_xpath_click('/html/body/div[1]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[2]/td[2]/div/iframe'))
        self.s_by_xpath_click('//*[@id="province"]/option[1]')
        self.s_by_xpath_click('//*[@id="city"]/option[1]')
        self.s_by_xpath_click('//*[@id="county"]/option[1]')
        self.s_by_name_input('zip',411302)
        self.s_by_name_input('addr')
        self.s_by_name_input('accept_name','')
        self.s_by_name_input('mobile',17726130771)
        self.s_by_name_input('phone',17726130771)
        self.s_by_xpath_click('//*[@id="address-form"]/table/tbody/tr[8]/td/input')

    def test_modify_address7(self):
        '''购物车确认订单信息时修改地址手机号格式错误'''
        self.s_by_link_click('修改地址')
        self.wd.switch_to.frame(self.s_by_xpath_click('/html/body/div[1]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[2]/td[2]/div/iframe'))
        self.s_by_xpath_click('//*[@id="province"]/option[1]')
        self.s_by_xpath_click('//*[@id="city"]/option[1]')
        self.s_by_xpath_click('//*[@id="county"]/option[1]')
        self.s_by_name_input('zip',411302)
        self.s_by_name_input('addr')
        self.s_by_name_input('accept_name')
        self.s_by_name_input('mobile')
        self.s_by_name_input('phone',17726130771)
        self.s_by_xpath_click('//*[@id="address-form"]/table/tbody/tr[8]/td/input')


    def test_modify_address8(self):
        '''购物车确认订单信息时修改地址不填写电话号码'''
        self.s_by_link_click('修改地址')
        self.wd.switch_to.frame(self.s_by_xpath_click('/html/body/div[1]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[2]/td[2]/div/iframe'))
        self.s_by_xpath_click('//*[@id="province"]/option[3]')
        self.s_by_xpath_click('//*[@id="city"]/option[2]')
        self.s_by_xpath_click('//*[@id="county"]/option[4]')
        self.s_by_name_input('addr')
        self.s_by_name_input('mobile',11726130771)
        self.s_by_name_input('phone','')
        self.s_by_xpath_click('//*[@id="address-form"]/table/tbody/tr[8]/td/input')
        self.assertEqual(self.wd.page_source.__contains__('手机号码格式错误'), True)



    def test_modify_address9(self):
        '''购物车确认订单信息时修改地址不按格式填写电话号码'''
        self.s_by_link_click('修改地址')
        self.wd.switch_to.frame(self.s_by_xpath_click('/html/body/div[1]/div/table/tbody/tr[2]/td[2]/div/table/tbody/tr[2]/td[2]/div/iframe'))
        self.s_by_xpath_click('//*[@id="province"]/option[3]')
        self.s_by_xpath_click('//*[@id="city"]/option[2]')
        self.s_by_xpath_click('//*[@id="county"]/option[4]')
        self.s_by_name_input('addr')
        self.s_by_name_input('mobile',11726130771)
        self.s_by_name_input('phone')
        self.s_by_xpath_click('//*[@id="address-form"]/table/tbody/tr[8]/td/input')
