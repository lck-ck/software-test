
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains

class ConfromOderTestCase(unittest.TestCase):
    def s_by_xpath_click(self, ref):
        e = self.wd.find_element(By.XPATH, ref)
        e.click()
        return e

    def s_by_link_click(self, ref):
        e = self.wd.find_element(By.LINK_TEXT, ref)
        e.click()
        return e

    def s_by_name_input(self, ref, string='adsgjfgdsj'):
        e = self.wd.find_element(By.NAME, ref)
        e.send_keys(string)
        return e

    def setUp(self) -> None:
        self.wd=webdriver.Chrome()
        self.ac = ActionChains(self.wd)
        url = 'http://127.0.0.1/tinyshop/index.php'
        self.wd.get(url)
        self.wd.implicitly_wait(10)
        self.wd.maximize_window()
        self.s_by_link_click('登录')
        self.s_by_name_input('account', 'test@qq.com')
        self.s_by_name_input('password', '123456')
        self.s_by_xpath_click('//*[@id="main"]/div/div/form/ul/li[4]/button')
        #测试商品至购物车
        self.s_by_xpath_click('//*[@id="main"]/div[3]/div[2]/div[2]/div[2]/ul/li[4]/dl/dd[1]/a')
        els = self.wd.find_elements(By.XPATH, '//*[@id="product-intro"]/div[3]/div/dl')
        for j in range(1,len(els)-2):
            self.s_by_xpath_click(f'//*[@id="product-intro"]/div[3]/div/dl[{j}]/dd/ul/li/span')

        self.s_by_link_click('加入购物车')
        self.ac.move_to_element(self.s_by_xpath_click('//*[@id="shopping-cart"]/i'))
        self.s_by_xpath_click('//*[@id="shopping-cart"]/div/div/a')
        self.s_by_link_click('立即结算')


    def tearDown(self) -> None:
        time.sleep(2)
        self.wd.quit()

    def test_submit_order(self):
        '''提交订单'''
        self.s_by_name_input('user_remark')
        self.s_by_xpath_click('//*[@id="main"]/div[2]/div/form/div[6]/p/input')
        contains__ = self.wd.page_source.__contains__('订单已成功提交')
        self.assertEqual(contains__,True)

    def test_check_order(self):
        '''查看订单'''
        self.s_by_name_input('user_remark')
        self.s_by_xpath_click('//*[@id="main"]/div[2]/div/form/div[6]/p/input')
        self.s_by_xpath_click('//*[@id="main"]/div[2]/form/div[2]/table/tbody/tr[1]/td[2]/a[1]')
        original_window = self.wd.current_window_handle
        for w in self.wd.window_handles:
            if w!=original_window:
                self.wd.close()
                self.wd.switch_to.window(w)
        contains__ = self.wd.page_source.__contains__('我的订单')
        self.assertEqual(contains__,True)

    def test_pay_order(self):
        '''支付订单'''
        self.s_by_name_input('user_remark')
        self.s_by_xpath_click('//*[@id="main"]/div[2]/div/form/div[6]/p/input')
        self.s_by_xpath_click('//*[@id="main"]/div[2]/form/div[3]/p/input')

        original_window = self.wd.current_window_handle
        for w in self.wd.window_handles:
            if w!=original_window:
                self.wd.close()
                self.wd.switch_to.window(w)
        contains__ = self.wd.page_source.__contains__('支付成功')
        self.assertEqual(contains__,True)

    def test_continue(self):
        '''继续购物'''
        self.s_by_name_input('user_remark')
        self.s_by_xpath_click('//*[@id="main"]/div[2]/div/form/div[6]/p/input')
        self.s_by_xpath_click('//*[@id="main"]/div[2]/form/div[3]/p/input')
        original_window = self.wd.current_window_handle
        for w in self.wd.window_handles:
            if w!=original_window:
                self.wd.close()
                self.wd.switch_to.window(w)
        self.s_by_link_click('继续购物')
        contains__ = self.wd.page_source.__contains__('你好:test@qq.com')
        self.assertEqual(contains__,True)


    def test_return_to_user_ccenter(self):
        '''返回到用户中心'''
        self.s_by_name_input('user_remark')
        self.s_by_xpath_click('//*[@id="main"]/div[2]/div/form/div[6]/p/input')
        self.s_by_xpath_click('//*[@id="main"]/div[2]/form/div[3]/p/input')
        original_window = self.wd.current_window_handle
        for w in self.wd.window_handles:
            if w!=original_window:
                self.wd.close()
                self.wd.switch_to.window(w)
        self.s_by_link_click('继续购物')
        contains__ = self.wd.page_source.__contains__('你好:test@qq.com')
        self.assertEqual(contains__,True)

