"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/16 23:20
# @Author : LuChangKai
# @File : shopping_1
# @Project : Utest1
"""
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.action_chains import ActionChains

# 前台登录的测试用例
import unittest


class Login_01_Testcase(unittest.TestCase):
    def setUp(self) -> None:
        # self.driver_path='E:/python_workspace/Utest1/web_driver/chromedriver.exe'
        # self.driver_server=Service(self.driver_path)

        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(10)

        self.driver.get('http://192.168.217.133:81/tinyshop/')

        self.driver.maximize_window()

    def tearDown(self) -> None:

        self.driver.quit()

    # 1.通过测试用例
    def test_login_pass(self):
        self.driver.find_element(By.XPATH, '//*[@id="header"]/div[1]/div/div[2]/ul/li[7]/a[1]').click()

        self.driver.find_element(By.XPATH, '//*[@id="account"]').send_keys("test@qq.com")
        self.driver.find_element(By.XPATH, '//*[@id="main"]/div/div/form/ul/li[2]/input').send_keys('admin123')

        self.driver.find_element(By.CLASS_NAME, 'btn.btn-main').click()

        page_source = self.driver.page_source
        if page_source.__contains__('你好:test@qq.com - TinyShop大型电子商务系统！'):
            # print('登陆成功')
            self.driver.find_element(By.XPATH, '//*[@id="main"]/div[3]/div[1]/div[2]/div[2]/ul/li[4]/dl/dt/a/img').click()

            self.driver.find_element(By.XPATH, '//*[@id="product-intro"]/div[3]/div/dl[1]/dd/ul/li[1]/span').click()

            self.driver.find_element(By.XPATH, '//*[@id="product-intro"]/div[3]/div/dl[2]/dd/ul/li[1]/span').click()

            self.driver.find_element(By.XPATH, '//*[@id="add-cart"]').click()

            def scrollBy_by(driver, h):
                js = 'window.scrollBy(0,{});'.format(h)  # 相对滚动
                driver.execute_script(js)

            scrollBy_by(self.driver, 300)
            # 鼠标悬停操作
            el = self.driver.find_element(By.XPATH, '//*[@id="shopping-cart"]')

            ActionChains(self.driver).move_to_element(el).perform()

            self.driver.find_element(By.XPATH, '//*[@id="shopping-cart"]/div/div/a').click()

            self.driver.find_element(By.XPATH, '//*[@id="main"]/div[2]/div[2]/p/a[2]').click()

            self.driver.find_element(By.XPATH, '//*[@id="main"]/div[2]/div/form/div[1]/ul/li').click()
            self.driver.find_element(By.XPATH, '//*[@id="main"]/div[2]/div/form/div[6]/p/input').click()
            self.driver.find_element(By.XPATH, '//*[@id="main"]/div[2]/form/div[3]/p/input').click()
        else:
            print('登陆失败')
    # 2.登录用户名错误
    def test_username_error(self):

        self.driver.find_element(By.LINK_TEXT, '登录').click()

        self.driver.find_element(By.NAME, 'account').send_keys('test@qq.co')

        self.driver.find_element(By.NAME, 'password').send_keys('admin123')
        self.driver.find_element(By.XPATH, '//*[@id="main"]/div/div/form/ul/li[4]/button').click()
        # 等于断言
        self.assertEqual(self.driver.page_source.__contains__('用户不存在'), True)

    # 3.登录的用户名为空
    def test_username_is_null(self):

        self.driver.find_element(By.LINK_TEXT, '登录').click()

        self.driver.find_element(By.NAME, 'account').send_keys('')

        self.driver.find_element(By.NAME, 'password').send_keys('admin123')
        self.driver.find_element(By.XPATH, '//*[@id="main"]/div/div/form/ul/li[4]/button').click()
        # 等于断言
        self.assertEqual(self.driver.page_source.__contains__('用户名为空'), True)

    # 4.密码错误
    def test_password_error(self):

        self.driver.find_element(By.LINK_TEXT, '登录').click()

        self.driver.find_element(By.NAME, 'account').send_keys('test@qq.com')

        self.driver.find_element(By.NAME, 'password').send_keys('admin1')
        self.driver.find_element(By.XPATH, '//*[@id="main"]/div/div/form/ul/li[4]/button').click()
        # 等于断言
        self.assertEqual(self.driver.page_source.__contains__('密码错误'), True)

    # 5.登录的密码为空
    def test_password_is_null(self):

        self.driver.find_element(By.LINK_TEXT, '登录').click()

        self.driver.find_element(By.NAME, 'account').send_keys('test@qq.com')

        self.driver.find_element(By.NAME, 'password').send_keys('')
        self.driver.find_element(By.XPATH, '//*[@id="main"]/div/div/form/ul/li[4]/button').click()
        # 等于断言
        self.assertEqual(self.driver.page_source.__contains__('密码不能为空'), True)

        # 未选择商品属性

    def test_selectAttribute_is_null(self):
        self.driver.find_element(By.XPATH, '//*[@id="header"]/div[1]/div/div[2]/ul/li[7]/a[1]').click()

        self.driver.find_element(By.XPATH, '//*[@id="account"]').send_keys("test@qq.com")
        self.driver.find_element(By.XPATH, '//*[@id="main"]/div/div/form/ul/li[2]/input').send_keys('admin123')

        self.driver.find_element(By.CLASS_NAME, 'btn.btn-main').click()

        page_source = self.driver.page_source
        if page_source.__contains__('你好:test@qq.com - TinyShop大型电子商务系统！'):
            # print('登陆成功')
            self.driver.find_element(By.XPATH,
                                     '//*[@id="main"]/div[3]/div[1]/div[2]/div[2]/ul/li[4]/dl/dt/a/img').click()

            # self.driver.find_element(By.XPATH, '//*[@id="product-intro"]/div[3]/div/dl[1]/dd/ul/li[1]/span').click()
            #
            # self.driver.find_element(By.XPATH, '//*[@id="product-intro"]/div[3]/div/dl[2]/dd/ul/li[1]/span').click()

            self.driver.find_element(By.XPATH, '//*[@id="add-cart"]').click()
            self.assertEqual(self.driver.page_source.__contains__('请选择您要购买的商品规格！'), True)

        else:
            print('登陆失败')


if __name__ == '__main__':
    unittest.main()
