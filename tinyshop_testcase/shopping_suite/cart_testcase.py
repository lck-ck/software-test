
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains

class CartTestCase(unittest.TestCase):
    def s_by_xpath_click(self, ref):
        e = self.wd.find_element(By.XPATH, ref)
        e.click()
        return e

    def s_by_link_click(self, ref):
        e = self.wd.find_element(By.LINK_TEXT, ref)
        e.click()
        return e

    def s_by_name_input(self, ref, string='adsgjfgdsj'):
        e = self.wd.find_element(By.NAME, ref)
        e.send_keys(string)
        return e

    def setUp(self) -> None:
        self.wd=webdriver.Chrome()
        self.ac = ActionChains(self.wd)
        url = 'http://127.0.0.1/tinyshop/index.php'
        self.wd.get(url)
        self.wd.implicitly_wait(10)
        self.wd.maximize_window()
        self.s_by_link_click('登录')
        self.s_by_name_input('account', 'test@qq.com')
        self.s_by_name_input('password', '123456')
        self.s_by_xpath_click('//*[@id="main"]/div/div/form/ul/li[4]/button')
        #测试商品至购物车
        self.s_by_xpath_click('//*[@id="main"]/div[3]/div[2]/div[2]/div[2]/ul/li[4]/dl/dd[1]/a')
        els = self.wd.find_elements(By.XPATH, '//*[@id="product-intro"]/div[3]/div/dl')
        for j in range(1,len(els)-2):
            self.s_by_xpath_click(f'//*[@id="product-intro"]/div[3]/div/dl[{j}]/dd/ul/li/span')

        self.s_by_link_click('加入购物车')

    def tearDown(self) -> None:
        time.sleep(2)
        self.wd.quit()

    def test_cancel(self):
        '''测试取消购物车中的商品'''
        self.ac.move_to_element(self.s_by_xpath_click('//*[@id="shopping-cart"]/i'))
        pre = self.wd.find_element(By.CLASS_NAME, 'dropdown').rect
        self.wd.find_element(By.CLASS_NAME,'ie6png').click()
        post = self.wd.find_element(By.XPATH, '//*[@id="shopping-cart"]/div').rect
        flag=False
        if pre != post:
             flag=True
        self.assertEqual(flag, True)


    def test_no_goods(self):
        '''测试购物车没有商品时，光标停留在购物车上'''
        self.ac.move_to_element(self.s_by_xpath_click('//*[@id="shopping-cart"]/i'))
        self.wd.find_element(By.CLASS_NAME, 'ie6png').click()
        self.assertEqual(self.wd.page_source.__contains__('购物车中还没有商品，赶紧选购吧'), True)

    def test_no_goods1(self):
        '''测试购物车没有商品时，点击去购物车'''
        self.ac.move_to_element(self.s_by_xpath_click('//*[@id="shopping-cart"]/i'))
        self.wd.find_element(By.CLASS_NAME, 'ie6png').click()
        self.s_by_xpath_click('//*[@id="shopping-cart"]/div/div/a')
        self.assertEqual(self.wd.page_source.__contains__('购物车内暂时没有商品'), True)

    def test_increase(self):
        '''购物车详情页增加商品数量'''
        self.ac.move_to_element(self.s_by_xpath_click('//*[@id="shopping-cart"]/i'))
        self.s_by_xpath_click('//*[@id="shopping-cart"]/div/div/a')
        self.s_by_name_input('buy_num','2')

    def test_increase1(self):
        '''购物车详情页增加商品数量超过存货量'''
        self.ac.move_to_element(self.s_by_xpath_click('//*[@id="shopping-cart"]/i'))
        self.s_by_xpath_click('//*[@id="shopping-cart"]/div/div/a')
        self.s_by_name_input('buy_num','99')
        self.assertEqual(self.wd.page_source.__contains__('最多购买'), True)

    def test_delete(self):
        '''购物车详情页删除商品'''
        self.ac.move_to_element(self.s_by_xpath_click('//*[@id="shopping-cart"]/i'))
        self.s_by_xpath_click('//*[@id="shopping-cart"]/div/div/a')
        time.sleep(2)
        self.s_by_xpath_click('//*[@id="22"]/td[8]/a')