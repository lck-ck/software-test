
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains

class AddToCartTestCase(unittest.TestCase):
    def s_by_xpath_click(self, ref):
        e = self.wd.find_element(By.XPATH, ref)
        e.click()
        return e

    def s_by_link_click(self, ref):
        e = self.wd.find_element(By.LINK_TEXT, ref)
        e.click()
        return e

    def s_by_name_input(self, ref, string='adsgjfgdsj'):
        e = self.wd.find_element(By.NAME, ref)
        e.send_keys(string)
        return e

    def setUp(self) -> None:
        self.wd=webdriver.Chrome()
        self.ac = ActionChains(self.wd)
        url = 'http://127.0.0.1/tinyshop/index.php'
        self.wd.get(url)
        self.wd.implicitly_wait(10)
        self.wd.maximize_window()
        self.s_by_link_click('登录')
        self.s_by_name_input('account', 'test@qq.com')
        self.s_by_name_input('password', '123456')
        self.s_by_xpath_click('//*[@id="main"]/div/div/form/ul/li[4]/button')
    def tearDown(self) -> None:
        time.sleep(2)
        self.wd.quit()

    def test_add_to_cart(self):
        '''测试商品至购物车'''
        self.s_by_xpath_click('//*[@id="main"]/div[3]/div[2]/div[2]/div[2]/ul/li[4]/dl/dd[1]/a')
        els = self.wd.find_elements(By.XPATH, '//*[@id="product-intro"]/div[3]/div/dl')
        for j in range(1,len(els)-2):
            self.s_by_xpath_click(f'//*[@id="product-intro"]/div[3]/div/dl[{j}]/dd/ul/li/span')

        self.s_by_link_click('加入购物车')
        self.ac.move_to_element(self.s_by_xpath_click('//*[@id="shopping-cart"]/i'))
        time.sleep(2)
        flag=self.s_by_xpath_click('//*[@id="22"]/div[2]').is_displayed()
        self.assertEqual(flag, True)

    def test_add_to_cart2(self):
        '''测试商品至购物车时不添加商品规格'''
        self.s_by_xpath_click('//*[@id="main"]/div[3]/div[2]/div[2]/div[2]/ul/li[4]/dl/dd[1]/a')
        self.s_by_link_click('加入购物车')
        self.assertEqual(self.wd.page_source.__contains__('请选择您要购买的商品规格'), True)

    def test_add_to_cart3(self):
        '''测试商品至购物车时只填写部分规格'''
        self.s_by_xpath_click('//*[@id="main"]/div[3]/div[2]/div[2]/div[2]/ul/li[4]/dl/dd[1]/a')
        els = self.wd.find_elements(By.XPATH, '//*[@id="product-intro"]/div[3]/div/dl')
        self.s_by_xpath_click(f'//*[@id="product-intro"]/div[3]/div/dl[{1}]/dd/ul/li/span')
        self.s_by_link_click('加入购物车')
        self.assertEqual(self.wd.page_source.__contains__('请选择您要购买的商品规格'), True)

    def test_add_to_cart4(self):
        '''测试商品至购物车购买量为0'''
        self.s_by_xpath_click('//*[@id="main"]/div[3]/div[2]/div[2]/div[2]/ul/li[4]/dl/dd[1]/a')
        els = self.wd.find_elements(By.XPATH, '//*[@id="product-intro"]/div[3]/div/dl')
        for j in range(1,len(els)-2):
            self.s_by_xpath_click(f'//*[@id="product-intro"]/div[3]/div/dl[{j}]/dd/ul/li/span')

        self.s_by_name_input('buy_num','0')
        self.s_by_link_click('加入购物车')
        self.ac.move_to_element(self.s_by_xpath_click('//*[@id="shopping-cart"]/i'))
        time.sleep(2)
        flag=self.s_by_xpath_click('//*[@id="22"]/div[2]').is_displayed()
        self.assertEqual(flag, True)


    def test_add_to_cart5(self):
        '''测试商品至购物车时超过允许购买的最大数量'''
        self.s_by_xpath_click('//*[@id="main"]/div[3]/div[2]/div[2]/div[2]/ul/li[4]/dl/dd[1]/a')
        els = self.wd.find_elements(By.XPATH, '//*[@id="product-intro"]/div[3]/div/dl')
        for j in range(1,len(els)-2):
            self.s_by_xpath_click(f'//*[@id="product-intro"]/div[3]/div/dl[{j}]/dd/ul/li/span')

        self.s_by_name_input('buy_num','999')
        self.s_by_link_click('加入购物车')
        self.assertEqual(self.wd.page_source.__contains__('连同购物车里的商品数量，超出了允许购买的最大量！'), True)
