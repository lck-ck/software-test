"""
# _*_ coding : utf_8 _*_
# @Time : 2022/11/21 11:15
# @Author : LuChangKai
# @File : run_all_case
# @Project : Utest1
"""
import unittest
import os
from common import HTMLTestRunner
import time

current_path = os.path.dirname(__file__)
start_path = os.path.join(current_path, '../tinyshop_testcase')

discover = unittest.defaultTestLoader.discover(start_dir=start_path, pattern='*testcase.py',
                                               top_level_dir=None)
# 主套件
main_suite = unittest.TestSuite()
main_suite.addTest(discover)
# unittest.main(defaultTest='main_suite')


# 报告每次覆盖的处理
reports_path = os.path.join(current_path, '../reports/')
now = time.strftime('%Y_%m_%d_%H_%M_%S')
file_name = 'result_' + now + '.html'
reports_file = reports_path + file_name

fp = open(reports_file, 'wb')
runner = HTMLTestRunner.HTMLTestRunner(stream=fp, title='测试报告', description='自动化软件测试：卢常锴')
runner.run(main_suite)

fp.close()
# from common import send_email_file
# send_email_file.send_email(file_name)